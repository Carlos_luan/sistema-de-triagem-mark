/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import java.util.Calendar;
import java.util.GregorianCalendar;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

/**
 *
 * @author carlos.luan / eduardo.luiz
 */
public class Etiqueta
{

    public void exec_etiqueta(WebDriver driver, String etiquetinha,String Local, String radical, String Complemento) throws InterruptedException
    {
        Actions action = new Actions(driver);
        Calendar calendar = new GregorianCalendar();
        int mes = calendar.get(Calendar.MONTH);
        if (etiquetinha.contains("ERRO EM TRIAGEM: PDF NÃO PESQUISÁVEL")||etiquetinha.contains("NÃO FOI POSSÍVEL LOCALIZAR ATO JUDICIAL ATUALIZADO"))
        {
            action.click(driver.findElement(By.xpath("//td/div/div"))).build().perform();
            action.contextClick(driver.findElement(By.xpath("//td[3]/div/a"))).build().perform();
            action.click(driver.findElement(By.partialLinkText("Etiquetar"))).build().perform();
            Thread.sleep(5000);
            driver.findElement(By.id("etiquetaConteudo-inputEl")).sendKeys(etiquetinha);
            Thread.sleep(4000);
            driver.findElement(By.partialLinkText("Salvar")).click();
        }
        else
        {
        // biblioteca Action que adiciona recursos extras de click e ela recebe o WebElement como paramentro

            // contextClick server para dar um click de acordo com o contexto do script que nesse caso Ã© apertar com o botÃ£o direito do mouse
            action.doubleClick(driver.findElement(By.xpath("//td[5]/div"))).build().perform();
            //action.click(driver.findElement(By.xpath("//td/div/div"))).build().perform();
            //action.contextClick(driver.findElement(By.xpath("//td[3]/div/a"))).build().perform();
            // como a função está em java script e alguns elementos surgem após um tempo de espera, esse comando faz aguardar 2 segundos para executar a proxima funÃ§Ã£o
            //Thread.sleep(4000);
            //action.click(driver.findElement(By.partialLinkText("Etiquetar"))).build().perform();
            Thread.sleep(5000);
            //driver.findElement(By.id("etiquetaConteudo-inputEl")).sendKeys(etiquetinha);
            driver.findElement(By.xpath("//fieldset[5]/div/span/div/table[4]/tbody/tr/td[2]/input")).clear();
            driver.findElement(By.xpath("//fieldset[5]/div/span/div/table[4]/tbody/tr/td[2]/input")).sendKeys(etiquetinha);
            driver.findElement(By.xpath("//div/div/span/div/table/tbody/tr/td[2]/textarea")).clear();
            if (etiquetinha.contains("PJE-FEDERAL-PARÁ"))
            {
                driver.findElement(By.xpath("//div/div/span/div/table/tbody/tr/td[2]/textarea")).sendKeys("ENVIAR PARA GEAC-COORD\n\'" + radical + "\' \'" + Complemento + "\'");
            }
            else
            {
                driver.findElement(By.xpath("//div/div/span/div/table/tbody/tr/td[2]/textarea")).sendKeys("BOT, RADICAL ENCONTRADO: \'" + radical + "\' COMPLEMENTO ENCONTRADO: \'" + Complemento + "\'"+"\nLocal:"+Local);
            }
            Thread.sleep(4000);
            driver.findElement(By.partialLinkText("Salvar")).click();

        }

    }
}

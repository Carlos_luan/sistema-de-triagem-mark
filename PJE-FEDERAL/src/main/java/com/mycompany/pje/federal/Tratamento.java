/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import java.text.Normalizer;

/**
 *
 * @author root2
 */
public class Tratamento
{

    public String removerAcento(String processo)
    {
        processo = Normalizer.normalize(processo, Normalizer.Form.NFD);
        processo = processo.replaceAll("[^\\p{ASCII}]", "");
        processo = processo.toUpperCase();
        processo = processo.replace(",", "");
        processo = processo.replace(":", "");
        processo = processo.replace("-", "");
        processo = processo.replace(".", "");
        processo = processo.replace("\"", "");
        processo = processo.replace("'", "");
        return processo;
    }
}

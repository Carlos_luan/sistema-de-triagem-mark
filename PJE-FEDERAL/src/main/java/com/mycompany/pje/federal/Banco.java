/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author carlos.luan
 */
public class Banco
{

    /*
     * É NECESSÁRIO FECHAR A CONECÇÃO DO BANCO DE DADOS PARA MANTER A
     * INTEGRIDADE DOS DADOS
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public void conectar()
    {
        try
        {
            // inicia a conecção com o banco de dados
            Connection connection = DriverManager.getConnection("jdbc:sqlite:bancoJEF.db");
            Connection connection2 = DriverManager.getConnection("jdbc:sqlite:bancoJF.db");
            Connection connection3 = DriverManager.getConnection("jdbc:sqlite:Relatorios.db");
            Connection connection4 = DriverManager.getConnection("jdbc:sqlite:Config.db");
            Statement comandoSql = connection.createStatement();
            Statement comandoSql2 = connection2.createStatement();
            Statement comandoSql3 = connection3.createStatement();
            Statement comandoSql4 = connection4.createStatement();
            // criando uma tabela
            comandoSql.execute("CREATE TABLE IF NOT EXISTS triagem (\n"
                    + "    RADICAL     VARCHAR (45)  NOT NULL,\n"
                    + "    COMPLEMENTO VARCHAR (100) NOT NULL,\n"
                    + "    ETIQUETA    VARCHAR (100) NOT NULL,\n"
                    + "    TIPO        STRING        NOT NULL\n"
                    + "                              DEFAULT 'TABELA',\n"
                    + "    PRIMARY KEY (\n"
                    + "        RADICAL,\n"
                    + "        COMPLEMENTO\n"
                    + "    )\n"
                    + ");");

            comandoSql2.execute("CREATE TABLE IF NOT EXISTS triagem (\n"
                    + "    RADICAL     VARCHAR (45)  NOT NULL,\n"
                    + "    COMPLEMENTO VARCHAR (100) NOT NULL,\n"
                    + "    ETIQUETA    VARCHAR (100) NOT NULL,\n"
                    + "    TIPO        STRING        NOT NULL\n"
                    + "                              DEFAULT 'TABELA',\n"
                    + "    PRIMARY KEY (\n"
                    + "        RADICAL,\n"
                    + "        COMPLEMENTO\n"
                    + "    )\n"
                    + ");");
            comandoSql3.execute("CREATE TABLE IF NOT EXISTS relatorio (\n"
                    + "    id       INTEGER        PRIMARY KEY AUTOINCREMENT,\n"
                    + "    decisao  VARCHAR (10000),\n"
                    + "    etiqueta VARCHAR (100),\n"
                    + "    Data     DATE\n"
                    + ");");
            comandoSql3.execute("CREATE TABLE IF NOT EXISTS relatorioVF (\n"
                    + "    id       INTEGER        PRIMARY KEY AUTOINCREMENT,\n"
                    + "    decisao  VARCHAR (10000),\n"
                    + "    etiqueta VARCHAR (100),\n"
                    + "    Data     DATE\n"
                    + ");");
            comandoSql4.execute("CREATE TABLE IF NOT EXISTS header (\n"
                    + "    headerDoc STRING (25) PRIMARY KEY\n"
                    + "                        NOT NULL\n"
                    + ");");
            //desconectando do banco de dados
            connection.close();
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public void inserir(Chaves chave)
    {
        try
        {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:bancoJEF.db");
            System.out.println("Conexão realizada !!!!");

            Statement statement = connection.createStatement();

            // inserindo registros
            statement.execute("INSERT INTO triagem (TIPO,RADICAL,COMPLEMENTO,ETIQUETA) VALUES ('" + chave.getTipo() + "', " + "'" + chave.getRadical() + "'" + ","
                    + "'" + chave.getComplemento() + "'" + ","
                    + "'" + chave.getEtiqueta() + "');");
            //desconectando do banco de dados
            connection.close();

        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public void inserirConfig(Headers chave)
    {
        try
        {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:Config.db");
            Statement statement = connection.createStatement();

            // inserindo registros
            statement.execute("INSERT INTO header (headerDoc)VALUES(\n"
                    + "'" + chave.getAssunto() + "');");
            //desconectando do banco de dados
            connection.close();

        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public void excluirConfig(Headers chave)
    {
        Connection connection;
        try
        {

            connection = DriverManager.getConnection("jdbc:sqlite:Config.db");
            Statement statement = connection.createStatement();
            statement.execute("DELETE FROM header\n"
                    + "      WHERE headerDoc = '" + chave.getAssunto() + "';");
            connection.close();
            JOptionPane.showMessageDialog(null, "Registro deletado!");

        }
        catch (SQLException ex)
        {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getMessage() + " \n Código do Erro: " + ex.getErrorCode());
        }
    }

    public void excluir(Chaves chave)
    {
        Connection connection;
        try
        {

            connection = DriverManager.getConnection("jdbc:sqlite:bancoJEF.db");
            Statement statement = connection.createStatement();
            statement.execute("DELETE FROM triagem  WHERE"
                    + "(RADICAL = '" + chave.getRadical() + "') AND "
                    + "(COMPLEMENTO = '" + chave.getComplemento() + "') ");
            connection.close();
            JOptionPane.showMessageDialog(null, "Registro deletado!");

        }
        catch (SQLException ex)
        {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getMessage() + " \n Código do Erro: " + ex.getErrorCode());
        }
    }

    public void alterarJEF(Chaves chave, String Radical, String Complemento, String Etiqueta, String tipo)
    {
        Connection connection;
        try
        {
            connection = DriverManager.getConnection("jdbc:sqlite:bancoJEF.db");
            Statement statement = connection.createStatement();
            statement.execute("UPDATE triagem SET RADICAL = '" + Radical
                    + "', COMPLEMENTO = '" + Complemento + "', ETIQUETA = '" + Etiqueta + "',TIPO = '" + tipo + "' WHERE "
                    + "RADICAL = '" + chave.getRadical()
                    + "' AND COMPLEMENTO = '" + chave.getComplemento() + "'");
            connection.close();
        }
        catch (SQLException ex)
        {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getMessage() + " \n Código do Erro: " + ex.getErrorCode());
        }
    }

    public void alterarConfig(Headers headerDoc,String headerDocould)
    {
        Connection connection;
        try
        {
            connection = DriverManager.getConnection("jdbc:sqlite:Config.db");
            Statement statement = connection.createStatement();
            statement.execute("UPDATE header\n"
                    + "   SET headerDoc = '"+headerDoc.getAssunto()+"'\n"
                    + " WHERE headerDoc = '"+headerDocould+"';");
            connection.close();
        }
        catch (SQLException ex)
        {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getMessage() + " \n Código do Erro: " + ex.getErrorCode());
        }
    }

    public void inserirVF(Chaves chave)
    {
        try
        {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:bancoJF.db");
            System.out.println("Conexão realizada !!!!");

            Statement statement = connection.createStatement();

            // inserindo registros
            statement.execute("INSERT INTO triagem (TIPO,RADICAL,COMPLEMENTO,ETIQUETA) VALUES ('" + chave.getTipo() + "', " + "'" + chave.getRadical() + "'" + ","
                    + "'" + chave.getComplemento() + "'" + ","
                    + "'" + chave.getEtiqueta() + "');");
            //desconectando do banco de dados
            connection.close();

        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public void excluir_citacao(Chaves chave)
    {
        Connection connection;
        try
        {

            connection = DriverManager.getConnection("jdbc:sqlite:bancoJF.db");
            Statement statement = connection.createStatement();
            statement.execute("DELETE FROM triagem  WHERE"
                    + "(RADICAL = '" + chave.getRadical() + "') AND "
                    + "(COMPLEMENTO = '" + chave.getComplemento() + "') ");
            connection.close();
            JOptionPane.showMessageDialog(null, "Registro deletado!");

        }
        catch (SQLException ex)
        {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getMessage() + " \n Código do Erro: " + ex.getErrorCode());
        }
    }

    public void alterarVF(Chaves chave, String Radical, String Complemento, String Etiqueta, String tipo)
    {
        Connection connection;
        try
        {
            connection = DriverManager.getConnection("jdbc:sqlite:bancoJF.db");
            Statement statement = connection.createStatement();
            statement.execute("UPDATE triagem SET RADICAL = '" + Radical
                    + "', COMPLEMENTO = '" + Complemento + "', ETIQUETA = '" + Etiqueta + "',TIPO = '" + tipo + "' WHERE "
                    + "RADICAL = '" + chave.getRadical()
                    + "' AND COMPLEMENTO = '" + chave.getComplemento() + "'");
            connection.close();
        }
        catch (SQLException ex)
        {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getMessage() + " \n Código do Erro: " + ex.getErrorCode());
        }
    }
}

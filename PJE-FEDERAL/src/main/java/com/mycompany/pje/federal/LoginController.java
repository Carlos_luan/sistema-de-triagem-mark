package com.mycompany.pje.federal;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Window;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.swing.JOptionPane;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class LoginController
{

    public WebDriver driver;
    public USer Usuario = new USer();
    private Stage primaryStage;
    public boolean btnTest = false;
    @FXML
    JFXTextArea log;
    @FXML
    JFXTextField Login;
    @FXML
    JFXPasswordField Senha;
    @FXML
    JFXSpinner SpinnerJef;
    @FXML
    JFXSpinner SpinnerVF;
    @FXML
    MenuItem triarAntigo;
    @FXML
    MenuItem AntigoVF;
    @FXML
    FontAwesomeIcon alert;
    @FXML
    JFXTextArea alerta;
    @FXML
    FontAwesomeIcon alertVF;
    @FXML
    JFXTextArea alertaVF;
//botão de login
    boolean antigo = false;
    boolean antigoVF = false;
    Alert erro = new Alert(AlertType.WARNING);

    public void handleEnterPressed(KeyEvent event) throws InterruptedException
    {
        if (event.getCode() == KeyCode.ENTER)
        {
            Login();
        }
    }

    @FXML
    private void VerificaAntigo()
    {
        if (antigo == false)
        {
            antigo = true;
            alerta.setText("O SISTEMA NÃO ESTÁ VERIFICANDO DATA");
            alert.setOpacity(1);
            triarAntigo.setText("Verificar Data");
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Informação");
            alert.initStyle(StageStyle.TRANSPARENT);
            alert.setHeaderText("Menssagem");
            alert.setContentText("Mark Está Configurado para triar Processos sem verificar data\nLembre-se: Com grandes poderes vem grandes responsabilidades");
            alert.showAndWait();
            // JOptionPane.showMessageDialog(null, "Mark Está Configurado para triar Processos sem verificar data\nLembre-se: Com grandes poderes vem grandes responsabilidades");
        }
        else
        {
            antigo = false;
            alerta.clear();
            triarAntigo.setText("Triar Antigo");
            alert.setOpacity(0);
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Informação");
            alert.initStyle(StageStyle.TRANSPARENT);
            alert.setHeaderText("Menssagem");
            alert.setContentText("Mark Está Configurado para triar processos de acordo com a data");
            alert.showAndWait();
            //JOptionPane.showMessageDialog(null, "Mark Está Configurado para triar processos de acordo com a data");
        }

    }

    @FXML
    private void VerificaAntigoVF()
    {
        if (antigoVF == false)
        {
            antigoVF = true;
            alertaVF.setText("O SISTEMA NÃO ESTÁ VERIFICANDO DATA");
            alertVF.setOpacity(1);
            AntigoVF.setText("Verificar Data");
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Informação");
            alert.initStyle(StageStyle.TRANSPARENT);
            alert.setHeaderText("Menssagem");
            alert.setContentText("Mark Está Configurado para triar Processos sem verificar data\nLembre-se: Com grandes poderes vem grandes responsabilidades");
            alert.showAndWait();
            // JOptionPane.showMessageDialog(null, "Mark Está Configurado para triar Processos sem verificar data\nLembre-se: Com grandes poderes vem grandes responsabilidades");
        }
        else
        {
            antigoVF = false;
            alertaVF.clear();
            AntigoVF.setText("Triar Antigo");
            alertVF.setOpacity(0);
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Informação");
            alert.initStyle(StageStyle.TRANSPARENT);
            alert.setHeaderText("Menssagem");
            alert.setContentText("Mark Está Configurado para triar processos de acordo com a data");
            alert.showAndWait();
            //  JOptionPane.showMessageDialog(null, "Mark Está Configurado para triar processos de acordo com a data");
        }

    }

    @FXML
    private void Login() throws InterruptedException
    {

        String S;
        USer Usuario = new USer();
        Usuario.setLogin(Login.getText());
        Usuario.setSenha(Senha.getText());
        S = Senha.getText();
        log.setText("-Login Iniciado!");
        try
        {
            if (System.getProperty("os.name").toUpperCase().contains("WINDOWS"))
            {
                System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
            }
            else
            {
                System.setProperty("webdriver.gecko.driver", "geckodriver.tar");
            }
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Driver de comunicação não encontrado, favor contatar o desenvolvedor.");
        }

        // Usuário contem login e senha encapsulados.
        final WebDriver driver = new FirefoxDriver();
        try
        {
            String url = "https://sapiens.agu.gov.br/login";
            driver.get(url);
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            Thread.sleep(3000);

            Usuario.exec_login(driver, Usuario);
            this.driver = driver;
            btnTest = true;
        }
        catch (Exception e)
        {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Informação");
            alert.setHeaderText("Menssagem");
            alert.setContentText("Erro ao Realizar Login:\n" + e.getLocalizedMessage().toString());
            alert.showAndWait();
        }

    }

//botão executa triagem
    @FXML
    @SuppressWarnings("ObjectEqualsNull")
    private void handleNew(ActionEvent event) throws IOException
    {
        URL url = getClass().getResource("/SOUNDS/fanfare.wav");
        AudioClip clip = Applet.newAudioClip(url);
        clip.play();
        startTask();
    }
//botão executa triagem vf

    @FXML
    @SuppressWarnings("ObjectEqualsNull")
    private void handleNew2(ActionEvent event) throws IOException
    {
        URL url = getClass().getResource("/SOUNDS/have_this.wav");
        AudioClip clip = Applet.newAudioClip(url);
        clip.play();
        startTask2();
    }

//botão retorna menu principal
    @FXML
    void RetornaMenu(ActionEvent event)
    {
        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        try
        {
            root = FXMLLoader.load(getClass().getResource("/fxml/Root.fxml"));
        }
        catch (IOException ex)
        {

        }
        Scene scene = new Scene(root);
        stage.setMinWidth(400);
        stage.setMinHeight(400);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
        stage.setTitle("Mark-II");

    }

    public void runTask()
    {
        log.setText(log.getText() + "\n-Triagem Iniciada!");
        SpinnerJef.setVisible(true);
        try
        {
            //instaciamento da classe  para buscar o processo a ser triado 
            buscaprocesso find = new buscaprocesso(driver);
            boolean teste = false;
            teste = find.find(driver, antigo);

            if (teste == false)
            {
                SpinnerJef.setVisible(false);
                URL url = getClass().getResource("/SOUNDS/special_item.wav");
                AudioClip clip = Applet.newAudioClip(url);
                clip.play();
                JOptionPane.showMessageDialog(null, "Não há tarefas à serem triadas");
                log.setText("-Triagem finalizada");

            }
            else if (teste == true)
            {
                SpinnerJef.setVisible(false);
                btnTest = false;
                URL url = getClass().getResource("/SOUNDS/error.wav");
                AudioClip clip = Applet.newAudioClip(url);
                clip.play();
         //       JOptionPane.showMessageDialog(null, "Erro de comunicação com plataforma Sapiens");
                log.setText("-Erro de comunicação com plataforma Sapiens!");

            }
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            if (btnTest == true)
            {
                SpinnerJef.setVisible(false);
                btnTest = false;
                URL url = getClass().getResource("/SOUNDS/error.wav");
                AudioClip clip = Applet.newAudioClip(url);
                clip.play();
              //  JOptionPane.showMessageDialog(null, "Erro de comunicação com plataforma Sapiens");
                log.setText("-Erro de comunicação com plataforma Sapiens!");

            }
            else
            {
                SpinnerJef.setVisible(false);
                URL url = getClass().getResource("/SOUNDS/error.wav");
                AudioClip clip = Applet.newAudioClip(url);
                clip.play();
                Platform.runLater(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        erro.setTitle("Informação");
                        erro.setHeaderText("Codigo: X001");
                        erro.setContentText("Erro ao Realizar Login:\n" + "Faça o Login primeiro");
                        erro.showAndWait();
                    }
                });
                log.setText("-Erro: Login Não Realizado!");

            }

        }

    }

    public void runTask2()
    {

        try
        {
            SpinnerVF.setVisible(true);
            log.setText(log.getText() + "\n-Triagem Iniciada!");
            //instaciamento da classe  para buscar o processo a ser triado 
            BuscaprocessoVF find = new BuscaprocessoVF(driver);
            boolean teste;
            teste = find.find(driver, antigoVF);

            if (teste == false)
            {
                SpinnerVF.setVisible(false);
                URL url = getClass().getResource("/SOUNDS/special_item.wav");
                AudioClip clip = Applet.newAudioClip(url);
                clip.play();
                JOptionPane.showMessageDialog(null, "Não há tarefas à serem triadas");
                log.setText("-Triagem finalizada");
            }
            else if (teste == true)
            {
                SpinnerVF.setVisible(false);
                URL url = getClass().getResource("/SOUNDS/error.wav");
                AudioClip clip = Applet.newAudioClip(url);
                clip.play();
                //JOptionPane.showMessageDialog(null, "Erro de comunicação com plataforma Sapiens");
                log.setText("-Erro de comunicação com plataforma Sapiens!");
            }
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            if (btnTest == true)
            {
                SpinnerVF.setVisible(false);
                URL url = getClass().getResource("/SOUNDS/error.wav");
                AudioClip clip = Applet.newAudioClip(url);
                clip.play();
             //   JOptionPane.showMessageDialog(null, "Erro de comunicação com plataforma Sapiens");
                log.setText("-Erro de comunicação com plataforma Sapiens!");
            }
            else
            {
                SpinnerVF.setVisible(false);
                URL url = getClass().getResource("/SOUNDS/error.wav");
                AudioClip clip = Applet.newAudioClip(url);
                clip.play();

                Platform.runLater(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        erro.setTitle("Informação");
                        erro.setHeaderText("Codigo: X001");
                        erro.setContentText("Erro ao Realizar Login:\n" + "Faça o Login primeiro");
                        erro.showAndWait();
                    }
                });

                //JOptionPane.showMessageDialog(null, "Faça o Login primeiro");
                log.setText("-Erro: Login Não realizado!");
            }

        }
    }

    public void startTask()
    {
        // Create a Runnable
        Runnable task = new Runnable()
        {
            public void run()
            {
                runTask();
            }
        };

        // Run the task in a background thread
        Thread backgroundThread = new Thread(task);
        // Terminate the running thread if the application exits
        backgroundThread.setDaemon(true);
        // Start the thread
        backgroundThread.start();

    }

    public void startTask2()
    {
        // Create a Runnable
        Runnable task = new Runnable()
        {
            public void run()
            {
                runTask2();
            }
        };

        // Run the task in a background thread
        Thread backgroundThread = new Thread(task);
        // Terminate the running thread if the application exits
        backgroundThread.setDaemon(true);
        // Start the thread
        backgroundThread.start();
    }

    @FXML
    public void Stop()
    {
        SpinnerVF.setVisible(false);
        SpinnerJef.setVisible(false);
        URL url = getClass().getResource("/SOUNDS/warp.wav");
        AudioClip clip = Applet.newAudioClip(url);
        clip.play();
        driver.quit();
    }

}

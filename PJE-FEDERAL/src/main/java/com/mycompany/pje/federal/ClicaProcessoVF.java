/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 *
 * @author root
 */
public class ClicaProcessoVF extends ClicaProcesso
{

    public ClicaProcessoVF()
    {
        super();
    }

    @Override
    public Resultado ExecTabela(WebDriver driver, Resultado resultado) throws SQLException
    {
        VerificaData verificadata = new VerificaData();
        WebElement TabelaTref = null;
        TabelaTref = driver.findElement(By.id("treeview-1015"));
        List<WebElement> TR = new ArrayList(TabelaTref.findElements(By.cssSelector("tr")));
        List<String> listinha = new ArrayList();
        Triagem triagem = new Triagem();
        Tratamento tratamento = new Tratamento();
        for (WebElement linha : TR)

        {
            List<WebElement> TD = new ArrayList(linha.findElements(By.cssSelector("td")));
            for (WebElement coluna : TD)
            {
                listinha.add(coluna.getText());
            }
        }

        for (int i = listinha.size() - 1; i > 0; i--)
        {
            resultado = triagem.findVF(listinha.get(i), "TABELA");
            if (verificadata.Verificar(listinha.get(i)))
            {
                if (!resultado.getEtiqueta().contains("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO"))
                {
                    return resultado;
                }
            }
        }
        return resultado;
    }

//    @Override
    public boolean executar(WebDriver driver, boolean VerificarAntigo) throws InterruptedException, UnsupportedFlavorException, IOException, SQLException
    {
        VerificaData verificadata = new VerificaData();
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        DataFlavor flavor = DataFlavor.stringFlavor;
        Actions actionzinho = new Actions(driver);
        boolean teste = false;
        WebElement TabelaTref = null;
        Thread.sleep(5000);

        TabelaTref = driver.findElement(By.id("treeview-1015"));
        //caso a tabela não seja carregadad por instabilidade ele tenta novamente depois de esperar mais 5 segundos
        List tarefasT = new ArrayList(TabelaTref.findElements(By.cssSelector("tr")));
        for (int i = tarefasT.size(); i > tarefasT.size() - 10; i--)
        {
            if (verificadata.Verificar(driver.findElement(By.xpath("//tr[" + i + "]/td[2]/div")).getText()) || VerificarAntigo)
            {
                driver.findElement(By.xpath("//tr[" + i + "]/td/div")).click();
                Thread.sleep(5000);
                WebElement Tela2 = driver.findElement(By.xpath("//*[@id=\"myiframe-body\"]"));
                Tela2.click();
                Actions action = new Actions(driver);
                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0061')).perform();
                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0063')).perform();
                String ato;

                clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                flavor = DataFlavor.stringFlavor;
                ato = clipboard.getData(flavor).toString();
                ato = ato.toUpperCase();
                Connection connection = DriverManager.getConnection("jdbc:sqlite:Config.db");
                PreparedStatement stmt;
                ResultSet resultSet;
                stmt = connection.prepareStatement("select *from header");
                resultSet = stmt.executeQuery();
                while (resultSet.next())
                {
                    if (ato.contains(resultSet.getString("headerDoc")))
                    {
                        if ((ato.contains("DECISÃO")
                                || ato.contains("ATO ORDINATÓRIO")
                                || ato.contains("ACÓRDÃO")
                                || ato.contains("SENTENÇA")
                                || ato.contains("DESPACHO")
                                || ato.contains("CITAÇÃO")
                                //COMENTAR CASO FOR COMPILAR PARA O PARÁ
                                || ato.contains("CERTIDÃO")
                                || ato.contains("MANDADO")
                                || ato.contains("FÍSICO MIGRADO PARA O PJE")))

                        {
                            if (ato.contains("INTIMAÇÃO VIA SISTEMA PJE")
                                    || ato.contains("MANDADO DE INTIMAÇÃO")
                                    || ato.contains("CITAÇÃO VIA SISTEMA")
                                    || ato.contains("CITAÇÃO VIA SISTEMA PJE")
                                    || ato.contains("MANDADO DE CITAÇÃO"))
                            {
                                //caso haja necessidade de oferecer contestatção dentro da citação
                                if (ato.contains("oferecer contestação".toUpperCase()))
                                {
                                    return true;
                                }
                                VerificarAntigo = true;
                            }
                            else if (ato.contains("EXTRATO DE DOSSIÊ PREVIDENCIÁRIO") 
                                    || ato.contains("DOSSIÊ MÉDICO"))
                            {

                            }
                            else
                            {
                                return true;
                            }

                        }
                    }
                }
            }
            if (i == 1)
            {
                return false;
            }
        }
        return false;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author carlos.luan / eduardo.luiz
 */
public class Triagem
{

    public List<List> lista_processos = new ArrayList();// lista para armazenar lista de localização dos processos
//Recebe o processo Judicial

    public Resultado find_JEF(String processo, String tipo)

    {
        Resultado resultado = new Resultado();
        boolean banco = false;
        while (banco == false)
        {

            try
            {
                Calendar calendar = new GregorianCalendar();
                int mes = calendar.get(Calendar.MONTH);
                int ano = calendar.get(Calendar.YEAR);
                int dia = calendar.get(Calendar.DAY_OF_MONTH);
                String data = ano + "-" + (mes + 1) + "-" + dia;
                Tratamento tratamento = new Tratamento();

                Connection connection = DriverManager.getConnection("jdbc:sqlite:bancoJEF.db");
                PreparedStatement stmt;
                processo = tratamento.removerAcento(processo);
                ResultSet resultSet;
                String sincronizar = "A intimação ainda está pendente de ciência no Tribunal";
                sincronizar = tratamento.removerAcento(sincronizar);
                stmt = connection.prepareStatement("select *from triagem where TIPO = '" + tipo + "'");
                resultSet = stmt.executeQuery();
                while (resultSet.next())
                {
                    String RADICAL = resultSet.getString("RADICAL");
                    RADICAL = tratamento.removerAcento(RADICAL);
                    String COMPLEMENTO = resultSet.getString("COMPLEMENTO");
                    COMPLEMENTO = tratamento.removerAcento(COMPLEMENTO);
                    String ETIQUETA = resultSet.getString("ETIQUETA");
                    if (processo.contains(RADICAL) && processo.contains(COMPLEMENTO))
                    {
                        resultado.setRadical(RADICAL);
                        resultado.setComplemetno(COMPLEMENTO);
                        resultado.setEtiqueta(ETIQUETA);

                        Connection connection2 = DriverManager.getConnection("jdbc:sqlite:Relatorios.db");
                        Statement statement = connection2.createStatement();
                        statement.execute("INSERT INTO relatorio (etiqueta,decisao,Data)\n"
                                + "VALUES ('" + ETIQUETA + "','" + processo + "','" + data + "');");
                        return resultado;

                    }
                }
                banco = true;
            }
            catch (SQLException ex)
            {
                banco = false;
                Logger.getLogger(Triagem.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        resultado.setEtiqueta("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO (PJE-JEF)");
        return resultado;
    }

    public Resultado findVF(String processo, String tipo) throws SQLException
    {
        {
            Calendar calendar = new GregorianCalendar();
            int mes = calendar.get(Calendar.MONTH);
            int ano = calendar.get(Calendar.YEAR);
            int dia = calendar.get(Calendar.DAY_OF_MONTH);
            String data = ano + "-" + (mes + 1) + "-" + dia;
            Tratamento tratamento = new Tratamento();
            processo = tratamento.removerAcento(processo);
            Resultado resultado = new Resultado();
            Connection connection = DriverManager.getConnection("jdbc:sqlite:bancoJF.db");
            PreparedStatement stmt;
            processo = tratamento.removerAcento(processo);
            ResultSet resultSet;
            String sincronizar = "A intimação ainda está pendente de ciência no Tribunal";
            sincronizar = tratamento.removerAcento(sincronizar);
            stmt = connection.prepareStatement("select *from triagem where TIPO = '" + tipo + "'");
            resultSet = stmt.executeQuery();
            while (resultSet.next())
            {
                String RADICAL = resultSet.getString("RADICAL");
                RADICAL = tratamento.removerAcento(RADICAL);
                String COMPLEMENTO = resultSet.getString("COMPLEMENTO");
                COMPLEMENTO = tratamento.removerAcento(COMPLEMENTO);
                String ETIQUETA = resultSet.getString("ETIQUETA");
                if (processo.contains(RADICAL) && processo.contains(COMPLEMENTO))
                {
                    resultado.setRadical(RADICAL);
                    resultado.setComplemetno(COMPLEMENTO);
                    resultado.setEtiqueta(ETIQUETA);
                    Connection connection2 = DriverManager.getConnection("jdbc:sqlite:Relatorios.db");
                    Statement statement = connection2.createStatement();
                    statement.execute("INSERT INTO relatorioVF (etiqueta,decisao,Data)\n"
                            + "VALUES ('" + ETIQUETA + "','" + processo + "','" + data + "');");
                    return resultado;
                }
                else if (processo.contains(sincronizar))
                {
                    resultado.setEtiqueta("A INTIMAÇÃO ESTÁ PENDENTE DE CIÊNCIA NO TRIBUNAL (PJE-VARA FEDERAL)");
                    resultado.setCheck(true);
                }

            }
            resultado.setEtiqueta("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO (PJE-VARA FEDERAL)");
            return resultado;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author root2
 */
public class EditController implements Initializable
{

    private Chaves chave;

    public EditController(Chaves chave)
    {
        this.chave = chave;
    }
    @FXML
    JFXTextField RadicalTxt, ComplTxt, etiquetaTxt;
    @FXML
    RadioButton LerMov;
    @FXML
    RadioButton LerDoc;

    final ToggleGroup group = new ToggleGroup();

    @FXML
    public boolean Alterar(ActionEvent event)
    {

        Banco banco = new Banco();
        String tipo = "TABELA";
        String radical = RadicalTxt.getText().toUpperCase().replace("'", "").replace("´", "");
        String complemento = ComplTxt.getText().toUpperCase().replace("'", "").replace("´", "");
        String etiqueta = etiquetaTxt.getText().toUpperCase().replace("'", "").replace("´", "");
        if ((radical.equals(null)) || complemento.equals(null) || etiqueta.equals(null))
        {
            JOptionPane.showMessageDialog(null, "Erro: Não insira palavras em branco");
            return false;
        }
        else if ((radical.equals("")) || complemento.equals("") || etiqueta.equals(""))
        {
            JOptionPane.showMessageDialog(null, "Erro: Não insira palavras em branco");
            return false;
        }
        else if ((radical.equals(" ")) || complemento.equals(" ") || etiqueta.equals(" "))
        {
            JOptionPane.showMessageDialog(null, "Erro: Não insira palavras em branco");
            return false;
        }
        else if (!LerDoc.isSelected() && !LerMov.isSelected())
        {
            JOptionPane.showMessageDialog(null, "Erro: SELECIONE UM TIPO DE TRIAGEM");
        }
        else
        {
            if (LerDoc.isSelected())
            {
                tipo = "DOC";
            }

            banco.alterarJEF(chave, radical, complemento, etiqueta, tipo);
            JOptionPane.showMessageDialog(null, "Etiqueta Alterada");
            Node node = (Node) event.getSource();
            Stage stage = (Stage) node.getScene().getWindow();
            Parent root = null;
            stage.close();
        }
        return true;
    }

    @FXML
    public void cancelar(ActionEvent event)
    {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        stage.close();
    }

    @FXML
    public void limpar()
    {
        RadicalTxt.clear();
        ComplTxt.clear();
        etiquetaTxt.clear();
    }

    @FXML
    public boolean alterarVF(ActionEvent event)
    {
        Banco banco = new Banco();
        String tipo = "TABELA";
        String radical = RadicalTxt.getText().toUpperCase().replace("'", "").replace("´", "");
        String complemento = ComplTxt.getText().toUpperCase().replace("'", "").replace("´", "");
        String etiqueta = etiquetaTxt.getText().toUpperCase().replace("'", "").replace("´", "");
        if ((radical.equals(null)) || complemento.equals(null) || etiqueta.equals(null))
        {
            JOptionPane.showMessageDialog(null, "Erro: Não insira palavras em branco");
            return false;
        }
        else if ((radical.equals("")) || complemento.equals("") || etiqueta.equals(""))
        {
            JOptionPane.showMessageDialog(null, "Erro: Não insira palavras em branco");
            return false;
        }
        else if ((radical.equals(" ")) || complemento.equals(" ") || etiqueta.equals(" "))
        {
            JOptionPane.showMessageDialog(null, "Erro: Não insira palavras em branco");
            return false;
        }
        else if (!LerDoc.isSelected() && !LerMov.isSelected())
        {
            JOptionPane.showMessageDialog(null, "Erro: SELECIONE UM TIPO DE TRIAGEM");
        }
        else
        {
            if (LerDoc.isSelected())
            {
                tipo = "DOC";
            }

            banco.alterarVF(chave, radical, complemento, etiqueta, tipo);
            JOptionPane.showMessageDialog(null, "Etiqueta Alterada");
            Node node = (Node) event.getSource();
            Stage stage = (Stage) node.getScene().getWindow();
            Parent root = null;
            stage.close();
        }
        return true;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        RadicalTxt.setText(chave.getRadical());
        ComplTxt.setText(chave.getComplemento());
        etiquetaTxt.setText(chave.getEtiqueta());
    }

}

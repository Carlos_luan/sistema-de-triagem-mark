/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.sql.SQLException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 *
 * @author carlos.luan / eduardo.luiz
 */
public class buscaarquivo
{

    public void sobe(WebDriver driver)
    {
        Actions action = new Actions(driver);
        action.sendKeys(Keys.PAGE_UP).build().perform();
        action.sendKeys(Keys.PAGE_UP).build().perform();

    }

    public String Verifica_processo(WebDriver driver) throws UnsupportedFlavorException, IOException, InterruptedException
    {

        MateriasPrev materia = new MateriasPrev();
        String Assunto = null;
        Actions action = new Actions(driver);
        WebElement Tela = driver.findElement(By.xpath("//*[@id=\"myiframe-body\"]"));
        Tela.click();
        action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0061')).perform();
        action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0063')).perform();
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        DataFlavor flavor = DataFlavor.stringFlavor;
        String Capa = clipboard.getData(flavor).toString();
        if (materia.materia(Capa))
        {
            return "true";
        }
        else
        {
            Assunto = "MATÉRIA NÃO PREVIDÊNCIARIA:" + Capa.substring(Capa.indexOf("Principal") + 9, Capa.indexOf("Informações") - 10);
            return Assunto;
        }
    }

    public Resultado copia_processo(WebDriver driver, int checkpoint,boolean antigo) throws InterruptedException, SQLException, UnsupportedFlavorException, IOException
    {
        Resultado resultado = new Resultado();
        ClicaProcesso click = new ClicaProcesso();
        String teste = "";
        boolean check;
        //verifico se a movimentação está pronta
        click.ClicaProcesso(driver);
        Tratamento tratamento = new Tratamento();
        if (checkpoint == 1)
        {
            resultado = click.ExecTabela(driver, resultado);            
            if (!resultado.getEtiqueta().contains("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO"))
            {   resultado.setLocal("Movimentação");
                return resultado;
            }
            check = resultado.getCheck();
        }
        else
        {

            check = click.executar(driver,antigo);
            if (!check)
            {
                resultado.setLocal("Documento");
                resultado.setEtiqueta("NÃO FOI POSSÍVEL LOCALIZAR ATO JUDICIAL ATUALIZADO");
                resultado.setRadical("");
                resultado.setComplemetno("");
                return resultado;
            }
            else
            {
                //Simula Ctrl+A (SELECIONA TODOS) / Ctrl+C (COPIAR)
                Actions action = new Actions(driver);
                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0061')).perform();
                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0063')).perform();
                Thread.sleep(3000);
                // colando documento do clipboard para uma string (CTRL+V) (COLAR)
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                DataFlavor flavor = DataFlavor.stringFlavor;
                teste = clipboard.getData(flavor).toString();
                // se ouver dados dentro do processo ele executa.
                if (teste.length() > 1)
                {
                    try
                    {
                        String processo = "";
                        String etiquetinha = "";
                        processo = clipboard.getData(flavor).toString();
                        //necessário encontrar a metade da string e soma-la para andar adiante do vetor
                        processo = processo.toUpperCase();
                        processo = tratamento.removerAcento(processo);
                        System.out.println("\n \t" + processo);
                        Triagem busca = new Triagem();
                        //recebe a etiqueta que foi identificada na triagem.
                        resultado = busca.find_JEF(processo, "DOC");
                        etiquetinha = resultado.getEtiqueta();
                        if (etiquetinha.contains("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO"));
                        {
                            for (int i = 0; i < 3; i++)
                            {
                                WebElement Tela2 = driver.findElement(By.xpath("//*[@id=\"myiframe-body\"]"));
                                Tela2.click();
                                sobe(driver);
                                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0061')).perform();
                                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0063')).perform();
                                action.keyUp(Keys.CONTROL).build().perform();
                                Thread.sleep(2000);
                                clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                                flavor = DataFlavor.stringFlavor;
                                processo = clipboard.getData(flavor).toString();
                                processo = processo.toUpperCase();
                                processo = tratamento.removerAcento(processo);
                                resultado = busca.find_JEF(processo, "DOC");
                                etiquetinha = resultado.getEtiqueta();
                                System.out.println("\n \t" + processo);
                                if (!etiquetinha.contains("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO"))
                                {
                                    return resultado;
                                }
                            }
                        }

                    }
                    catch (UnsupportedFlavorException e)
                    {
                        System.out.println(e);
                    }
                    catch (IOException e)
                    {
                        System.out.println(e);
                    }
                }
                else
                {
                    resultado.setEtiqueta("ERRO EM TRIAGEM: PDF NÃO PESQUISÁVEL");
                    return resultado;
                }
            }
        }

        /*
         * necessário usar comandos de teclado para copiar o arquivo do processo
         * para
         * a memória principal.
         */
        /*
         *
         *
         */
        return resultado;
    }
}

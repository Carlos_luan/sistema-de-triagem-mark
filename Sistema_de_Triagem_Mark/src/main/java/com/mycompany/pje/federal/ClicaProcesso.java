/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 *
 * @author carlos luan / eduardo rosa
 */
public class ClicaProcesso
{

    public boolean provJuri(String processo) throws SQLException
    {
        Connection connection = DriverManager.getConnection("jdbc:sqlite:Config.db");
        PreparedStatement stmt;
        ResultSet resultSet;
        stmt = connection.prepareStatement("SELECT * FROM ProvJuri");
        resultSet = stmt.executeQuery();
        while (resultSet.next())
        {
            if (processo.contains(resultSet.getString(1)))
            {
                return true;
            }
        }
        return false;
    }

    public boolean execao(String processo) throws SQLException
    {
        Connection connection = DriverManager.getConnection("jdbc:sqlite:Config.db");
        PreparedStatement stmt;
        ResultSet resultSet;
        stmt = connection.prepareStatement("SELECT * FROM Execao");
        resultSet = stmt.executeQuery();
        while (resultSet.next())
        {
            if (processo.contains(resultSet.getString(1)))
            {
                return false;
            }
        }
        return true;
    }

    public void ClicaProcesso(WebDriver driver) throws InterruptedException, UnsupportedFlavorException, IOException
    {
        WebElement TabelaTref = null;
        boolean teste = false;
        //ALTERADO de 5000 para 2000
        Thread.sleep(2000);
        //cria uma lista dos arquivos dentro do processo
        try
        {
            TabelaTref = driver.findElement(By.id("treeview-1015"));
            // loading do carregamento do arquivo
        }
        catch (Exception e)
        {

            for (int j = 0; j < 2; j++)
            {
                for (int k = 0; k < 2; k++)
                {
                    Thread.sleep(2000);
                    try
                    {
                        TabelaTref = driver.findElement(By.id("treeview-1015"));
                        teste = true;
                        break;
                    }
                    catch (Exception f)
                    {

                    }
                }
                if (teste = true)
                {
                    break;
                }
                else
                {
                    driver.navigate().refresh();
                }
            }
        }
    }

    // Qnd a triagem está sendo realizada somente na movimentação, o Mark etiqueta vazio qnd n encontra frase chave
    public Resultado ExecTabela(WebDriver driver, Resultado resultado, boolean antigo) throws SQLException
    {
        VerificaData verificadata = new VerificaData();
        WebElement TabelaTref = null;
        TabelaTref = driver.findElement(By.id("treeview-1015"));
        List<WebElement> TR = new ArrayList(TabelaTref.findElements(By.cssSelector("tr")));
        List<String> listinha = new ArrayList();
        Triagem triagem = new Triagem();
        //Tratamento tratamento = new Tratamento(); inutilizado
        for (WebElement linha : TR)
        {
            List<WebElement> TD = new ArrayList(linha.findElements(By.cssSelector("td")));
            for (WebElement coluna : TD)
            {
                listinha.add(coluna.getText());
            }
        }

        
        for (int i = listinha.size() - 1; i > 0; i--)
        {
            resultado = triagem.find_JEF(listinha.get(i), "TABELA");
            if (verificadata.Verificar(listinha.get(i)) || antigo)
            {
                if (!resultado.getEtiqueta().contains("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO"))
                {
                    return resultado;
                }
            }
        }
        return resultado;
    }

    public boolean executar(WebDriver driver, boolean VerificarAntigo) throws InterruptedException, UnsupportedFlavorException, IOException, SQLException
    {
        VerificaData verificadata = new VerificaData();
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        DataFlavor flavor = DataFlavor.stringFlavor;
        Actions actionzinho = new Actions(driver);
        boolean teste = false;
        WebElement TabelaTref = null;
        Thread.sleep(2000);

        TabelaTref = driver.findElement(By.id("treeview-1015"));
        //caso a tabela não seja carregadad por instabilidade ele tenta novamente depois de esperar mais 5 segundos
        List tarefasT = new ArrayList(TabelaTref.findElements(By.cssSelector("tr")));

        int limite = 10;
        // clica no ultimo processo, o despacho do juiz sempre vai ser o ultimo arquivo dentro de uma tarefa
        for (int i = tarefasT.size(); i > tarefasT.size() - limite; i--)
        {
            try {
            //System.out.println(driver.findElement(By.xpath("//tr[" + i + "]/td[2]/div")).getText());
            if (verificadata.Verificar(driver.findElement(By.xpath("//tr[" + i + "]/td[2]/div")).getText()) || VerificarAntigo)
            {
                driver.findElement(By.xpath("//tr[" + i + "]/td/div")).click();
                Thread.sleep(2000);
                WebElement Tela2 = driver.findElement(By.xpath("//*[@id=\"myiframe-body\"]"));
                Tela2.click();
                Actions action = new Actions(driver);
                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0061')).perform();
                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0063')).perform();
                String ato;

                clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                flavor = DataFlavor.stringFlavor;
                ato = clipboard.getData(flavor).toString();
                ato = ato.toUpperCase();
                String atinho = ato;
                
                System.out.println("Atinho: " + atinho);
                
                Connection connection = DriverManager.getConnection("jdbc:sqlite:Config.db");
                PreparedStatement stmt;
                ResultSet resultSet;
                stmt = connection.prepareStatement("SELECT * FROM header");
                resultSet = stmt.executeQuery();
                while (resultSet.next())
                {
                    if (ato.contains(resultSet.getString("headerDoc")))
                    {
                        if (provJuri(ato) && execao(ato))
                        {
                            //Removido por obrigar o sistema a buscar as palavras descritas abaixo, resquicio de codigo do Mark Nila
                            //if (ato.contains("INTIMAÇÃO") || ato.contains("CITAÇÃO"))
                            //{
                                Tratamento tratamento = new Tratamento();
                                Connection conec = DriverManager.getConnection("jdbc:sqlite:bancoJEF.db");
                                atinho = tratamento.removerAcento(atinho);
                                PreparedStatement st = conec.prepareStatement("SELECT * FROM triagem WHERE TIPO = 'DOC'");
                                ResultSet Set = st.executeQuery();
                                while (Set.next())
                                {
                                    String RADICAL = Set.getString("RADICAL");
                                    RADICAL = tratamento.removerAcento(RADICAL);
                                    String COMPLEMENTO = Set.getString("COMPLEMENTO");
                                    COMPLEMENTO = tratamento.removerAcento(COMPLEMENTO);
                                    String ETIQUETA = Set.getString("ETIQUETA");
                                    if (atinho.contains(RADICAL) && atinho.contains(COMPLEMENTO))
                                    {
                                        return true;
                                    }
                                }
                                VerificarAntigo = true;
                            //}
                            
                        }
                        else
                        {
                                return true;
                        }
                    }
                }
            } else {
                limite++;
            }
            if (i == 1)
            {
                return false;
            }
            } catch (Exception ex) {
                Thread.sleep(2000);
                driver.findElement(By.id("button-1005-btnEl")).click();
            }
        }
        return false;
    }

}

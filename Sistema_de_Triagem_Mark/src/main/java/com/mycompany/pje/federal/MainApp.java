package com.mycompany.pje.federal;

import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author carlos.luan / eduardo.luiz
 * @author felipe.marques / gabriel.ramos / rafael.almeida / adriano.vilhena
 */
public class MainApp extends Application
{

    public  WebDriver driver;
    public USer Usuario = new USer();
    private Stage primaryStage;

//abre a tela de usuário para login e triagem 
    @FXML
    void goTo(ActionEvent event)
    {

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        try
        {
            root = FXMLLoader.load(getClass().getResource("/fxml/login.fxml"));
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();

    }
    @FXML
    public void alterar(ActionEvent event) throws IOException
    {
        
       Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        try
        {
            root = FXMLLoader.load(getClass().getResource("/fxml/AssuntoPrev.fxml"));
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();
    }
    @FXML
    void goTo2(ActionEvent event)
    {

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        try
        {
            root = FXMLLoader.load(getClass().getResource("/fxml/JanelaAdmin.fxml"));
        }
        catch (IOException ex)
        {

        }

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.setTitle("Banco de Dados para Triagem");
        stage.show();

    }

    @FXML
    void goTo3(ActionEvent event)
    {
        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        try
        {
            root = FXMLLoader.load(getClass().getResource("/fxml/JanelaAdminJF.fxml"));
        }
        catch (IOException ex)
        {

        }

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.setTitle("Banco de Dados para Triagem");
        stage.show();
    }
    @FXML
    void goTo4(ActionEvent event)
    {

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        try
        {
            root = FXMLLoader.load(getClass().getResource("/fxml/BancoEstadual.fxml"));
        }
        catch (IOException ex)
        {

        }

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.setTitle("Banco de Dados para Triagem");
        stage.show();

    }
// carrega a cena da interface root

    @Override
    public void start(Stage primaryStage) throws Exception
    {

        // Carrega o layout FXML
        Pane root = FXMLLoader.load(getClass().getResource("/fxml/Root.fxml"));

        // Cria a cena
        Scene scene = new Scene(root);

        // Define parâmetros para a janela
        primaryStage.setMinWidth(400);
        primaryStage.setMinHeight(400);
        primaryStage.getIcons().add(new Image("/fxml/ironman-icon2.png"));
        primaryStage.setTitle("Mark-II");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();

    }

    public static void main(String[] args) throws Exception
    {
		// Inicializa o JavaFX
        // Inicializa o JavaFX
        Banco bancoChaves = new Banco();
        bancoChaves.conectar();
        launch(args);

    }

}
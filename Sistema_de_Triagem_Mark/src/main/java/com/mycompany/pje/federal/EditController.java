/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author root2
 */
public class EditController implements Initializable
{

    private Chaves chave;

    public EditController(Chaves chave)
    {
        this.chave = chave;
    }
    @FXML
    JFXTextField RadicalTxt, ComplTxt, etiquetaTxt;
    @FXML
    RadioButton LerMov;
    @FXML
    RadioButton LerDoc;
    @FXML
    RadioButton P1;
    @FXML
    RadioButton P2;
    @FXML
    RadioButton P3;
    @FXML
    RadioButton P4;
    @FXML
    JFXButton salvar;
    @FXML
    JFXButton cancelar;
    @FXML
    JFXButton eraser;
    
    final ToggleGroup peso = new ToggleGroup();
    
    final ToggleGroup group = new ToggleGroup();

    @FXML
    public boolean Alterar(ActionEvent event)
    {

        Banco banco = new Banco();
        String tipo = "TABELA";
        String radical = RadicalTxt.getText().toUpperCase().replace("'", "").replace("´", "");
        String complemento = ComplTxt.getText().toUpperCase().replace("'", "").replace("´", "");
        String etiqueta = etiquetaTxt.getText().toUpperCase().replace("'", "").replace("´", "");
        String prioridade = Peso();
        if ((radical.equals(null)) || complemento.equals(null) || etiqueta.equals(null))
        {
            JOptionPane.showMessageDialog(null, "ERRO: Não insira palavras em branco");
            return false;
        }
        else if ((radical.equals("")) || complemento.equals("") || etiqueta.equals(""))
        {
            JOptionPane.showMessageDialog(null, "ERRO: Não insira palavras em branco");
            return false;
        }
        else if ((radical.equals(" ")) || complemento.equals(" ") || etiqueta.equals(" "))
        {
            JOptionPane.showMessageDialog(null, "ERRO: Não insira palavras em branco");
            return false;
        }
        else if (!LerDoc.isSelected() && !LerMov.isSelected())
        {
            JOptionPane.showMessageDialog(null, "ERRO: Selecione um tipo de triagem");
        }
        else if (!P1.isSelected() && !P2.isSelected() && !P3.isSelected() && !P4.isSelected())
        {
            JOptionPane.showMessageDialog(null, "ERRO: Selecione um peso");
        }
        else
        {
            if (LerDoc.isSelected())
            {
                tipo = "DOC";
            }

            banco.alterarJEF(chave, radical, complemento, etiqueta, tipo, prioridade);
            JOptionPane.showMessageDialog(null, "Etiqueta Alterada");
            Node node = (Node) event.getSource();
            Stage stage = (Stage) node.getScene().getWindow();
            Parent root = null; 
            stage.close();
        }
        return true;
    }

    @FXML
    public void cancelar(ActionEvent event)
    {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        stage.close();
    }

    @FXML
    public void limpar()
    {
        RadicalTxt.clear();
        ComplTxt.clear();
        etiquetaTxt.clear();
    }

    @FXML
    public boolean alterarVF(ActionEvent event)
    {
        Banco banco = new Banco();
        String tipo = "TABELA";
        String radical = RadicalTxt.getText().toUpperCase().replace("'", "").replace("´", "");
        String complemento = ComplTxt.getText().toUpperCase().replace("'", "").replace("´", "");
        String etiqueta = etiquetaTxt.getText().toUpperCase().replace("'", "").replace("´", "");
        String prioridade = Peso();
        if ((radical.equals(null)) || complemento.equals(null) || etiqueta.equals(null))
        {
            JOptionPane.showMessageDialog(null, "ERRO: Não insira palavras em branco");
            return false;
        }
        else if ((radical.equals("")) || complemento.equals("") || etiqueta.equals(""))
        {
            JOptionPane.showMessageDialog(null, "ERRO: Não insira palavras em branco");
            return false;
        }
        else if ((radical.equals(" ")) || complemento.equals(" ") || etiqueta.equals(" "))
        {
            JOptionPane.showMessageDialog(null, "ERRO: Não insira palavras em branco");
            return false;
        }
        else if (!LerDoc.isSelected() && !LerMov.isSelected())
        {
            JOptionPane.showMessageDialog(null, "ERRO: Selecione um tipo de triagem");
        }
        else if (!P1.isSelected() && !P2.isSelected() && !P3.isSelected() && !P4.isSelected())
        {
            JOptionPane.showMessageDialog(null, "ERRO: Selecione um peso");
        }
        else
        {
            if (LerDoc.isSelected())
            {
                tipo = "DOC";
            }

            banco.alterarVF(chave, radical, complemento, etiqueta, tipo, prioridade);
            JOptionPane.showMessageDialog(null, "Etiqueta Alterada");
            Node node = (Node) event.getSource();
            Stage stage = (Stage) node.getScene().getWindow();
            Parent root = null;
            stage.close();
        }
        return true;
    }

    @FXML
    public boolean alterarEST(ActionEvent event)
    {
        Banco banco = new Banco();
        String tipo = "TABELA";
        String radical = RadicalTxt.getText().toUpperCase().replace("'", "").replace("´", "");
        String complemento = ComplTxt.getText().toUpperCase().replace("'", "").replace("´", "");
        String etiqueta = etiquetaTxt.getText().toUpperCase().replace("'", "").replace("´", "");
        String prioridade = Peso();
        if ((radical.equals(null)) || complemento.equals(null) || etiqueta.equals(null))
        {
            JOptionPane.showMessageDialog(null, "ERRO: Não insira palavras em branco");
            return false;
        }
        else if ((radical.equals("")) || complemento.equals("") || etiqueta.equals(""))
        {
            JOptionPane.showMessageDialog(null, "ERRO: Não insira palavras em branco");
            return false;
        }
        else if ((radical.equals(" ")) || complemento.equals(" ") || etiqueta.equals(" "))
        {
            JOptionPane.showMessageDialog(null, "ERRO: Não insira palavras em branco");
            return false;
        }
        else if (!LerDoc.isSelected() && !LerMov.isSelected())
        {
            JOptionPane.showMessageDialog(null, "ERRO: Selecione um tipo de triagem");
        }
        else if (!P1.isSelected() && !P2.isSelected() && !P3.isSelected() && !P4.isSelected())
        {
            JOptionPane.showMessageDialog(null, "ERRO: Selecione um peso");
        }
        else
        {
            if (LerDoc.isSelected())
            {
                tipo = "DOC";
            }

            banco.alterarEST(chave, radical, complemento, etiqueta, tipo, prioridade);
            JOptionPane.showMessageDialog(null, "Etiqueta Alterada");
            Node node = (Node) event.getSource();
            Stage stage = (Stage) node.getScene().getWindow();
            Parent root = null;
            stage.close();
        }
        return true;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        RadicalTxt.setText(chave.getRadical());
        ComplTxt.setText(chave.getComplemento());
        etiquetaTxt.setText(chave.getEtiqueta());
    }
    public String Peso()
    {
        if(P1.isSelected()){
            return "1";
        }else if(P2.isSelected()){
            return "2";
        }else if(P3.isSelected()){
            return "3";
        }else if(P4.isSelected()){
            return "4";
        }
        return null;
    }//Alterado
}

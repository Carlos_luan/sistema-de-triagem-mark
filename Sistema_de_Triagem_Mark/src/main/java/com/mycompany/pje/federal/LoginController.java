package com.mycompany.pje.federal;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Window;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.swing.JOptionPane;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class LoginController {

    public WebDriver driver;
    public USer Usuario = new USer();
    private Stage primaryStage;
    public boolean btnTest = false;
    @FXML
    JFXTextArea log;
    @FXML
    JFXSpinner Progress;
    @FXML
    JFXTextField Login;
    @FXML
    JFXPasswordField Senha;
    @FXML
    JFXSpinner SpinnerJef;
    @FXML
    JFXSpinner SpinnerVF;
    @FXML
    JFXSpinner SpinnerEst;
    @FXML
    MenuItem triarAntigo;
    @FXML
    MenuItem AntigoVF;
    @FXML
    MenuItem AntigoEst;
    /*
    @FXML
    MenuItem Pericial;
    */
    @FXML
    FontAwesomeIcon alert;
    @FXML
    FontAwesomeIcon alertVF;
    @FXML
    FontAwesomeIcon alertEst;
    @FXML
    FontAwesomeIcon alertPericial;
    @FXML
    JFXTextArea alerta;
    @FXML
    JFXTextArea alertaVF;
    @FXML
    JFXTextArea alertaEst;
    @FXML
    JFXTextArea alertaPericial;
    @FXML
    JFXToggleButton tc;
    @FXML
    JFXToggleButton sequencial;
    @FXML
    JFXToggleButton documento;
    

    final ToggleGroup group = new ToggleGroup();
//botão de login
    boolean seq = false;
    boolean doc = false;
    boolean trc = false;
    boolean antigo = false;
    boolean antigoVF = false;
    boolean antigoEst = false;
    boolean pericial = false;
    Alert erro = new Alert(AlertType.WARNING);

    public void handleEnterPressed(KeyEvent event) throws InterruptedException {
        if (event.getCode() == KeyCode.ENTER) {
            Login();
        }
    }
    /*
    @FXML
    private void LaudoPericial() {
        if (pericial == false) {
            pericial = true;
            alertaPericial.setText("Buscando EXLUSIVAMENTE por Laudo Pericial em pastas");
            alertPericial.setOpacity(1);
            Pericial.setText("Verificação Padrão");
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Informação");
            alert.initStyle(StageStyle.TRANSPARENT);
            alert.setHeaderText("Menssagem");
            alert.setContentText("ATENÇÃO\nO sistema de triagem está buscando EXCLUSIVAMENTE por Laudos Pericias dentro de pastas.");
            alert.showAndWait();
        } else {
            pericial = false;
            alertaPericial.clear();
            Pericial.setText("Buscar por Laudo Pericial");
            alertPericial.setOpacity(0);
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Informação");
            alert.initStyle(StageStyle.TRANSPARENT);
            alert.setHeaderText("Menssagem");
            alert.setContentText("Mark está configurado para triar processos de acordo com a data.");
            alert.showAndWait();
        }
    }
    */

    @FXML
    private void VerificaAntigo() {
        if (antigo == false) {
            antigo = true;
            alerta.setText("O Sistema NÃO está verificando data.");
            alert.setOpacity(1);
            triarAntigo.setText("Verificar Data");
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Informação");
            alert.initStyle(StageStyle.TRANSPARENT);
            alert.setHeaderText("Menssagem");
            alert.setContentText("Mark está configurado para triar processos SEM VERIFICAR DATA.\nLembre-se: Com grandes poderes vem grandes responsabilidades.");
            alert.showAndWait();
        } else {
            antigo = false;
            alerta.clear();
            triarAntigo.setText("Triar Antigo");
            alert.setOpacity(0);
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Informação");
            alert.initStyle(StageStyle.TRANSPARENT);
            alert.setHeaderText("Menssagem");
            alert.setContentText("Mark está configurado para triar processos de acordo com a data.");
            alert.showAndWait();
        }

    }
    
    @FXML
    private void VerificaAntigoVF() {
        if (antigoVF == false) {
            antigoVF = true;
            alertaVF.setText("O Sistema NÃO está verificando data.");
            alertVF.setOpacity(1);
            AntigoVF.setText("Verificar Data");
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Informação");
            alert.initStyle(StageStyle.TRANSPARENT);
            alert.setHeaderText("Mensagem");
            alert.setContentText("Mark está configurado para triar processos SEM VERIFICAR DATA.\nLembre-se: Com grandes poderes vem grandes responsabilidades.");
            alert.showAndWait();
        } else {
            antigoVF = false;
            alertaVF.clear();
            AntigoVF.setText("Triar Antigo");
            alertVF.setOpacity(0);
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Informação");
            alert.initStyle(StageStyle.TRANSPARENT);
            alert.setHeaderText("Mensagem");
            alert.setContentText("Mark está configurado para triar processos de acordo com a data.");
            alert.showAndWait();
        }

    }
    
    @FXML
    private void VerificaAntigoEst() {
        if (antigoEst == false) {
            antigoEst = true;
            alertaEst.setText("O Sistema NÃO está verificando data.");
            alertEst.setOpacity(1);
            AntigoEst.setText("Verificar Data");
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Informação");
            alert.initStyle(StageStyle.TRANSPARENT);
            alert.setHeaderText("Mensagem");
            alert.setContentText("Mark está configurado para triar processos SEM VERIFICAR DATA.\nLembre-se: Com grandes poderes vem grandes responsabilidades.");
            alert.showAndWait();
        } else {
            antigoEst = false;
            alertaEst.clear();
            AntigoEst.setText("Triar Antigo");
            alertEst.setOpacity(0);
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Informação");
            alert.initStyle(StageStyle.TRANSPARENT);
            alert.setHeaderText("Mensagem");
            alert.setContentText("Mark está configurado para triar processos de acordo com a data.");
            alert.showAndWait();
        }

    }

    @FXML
    private void Login() throws InterruptedException {

        String S;
        USer Usuario = new USer();
        Usuario.setLogin(Login.getText());
        Usuario.setSenha(Senha.getText());
        S = Senha.getText();
        log.setText("- Login Iniciado!");
        try {
            if (System.getProperty("os.name").toUpperCase().contains("WINDOWS")) {
                System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
            } else {
                System.setProperty("webdriver.gecko.driver", "geckodriver.tar");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Driver de comunicação não encontrado, favor contatar o desenvolvedor.");
        }

        // Usuário contem login e senha encapsulados.
        final WebDriver driver = new FirefoxDriver();
        try {
            String url = "https://sapiens.agu.gov.br/login";
            driver.get(url);
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            Thread.sleep(1000);

            Usuario.exec_login(driver, Usuario);
            this.driver = driver;
            btnTest = true;
        } catch (Exception e) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Informação");
            alert.setHeaderText("Menssagem");
            alert.setContentText("Erro ao Realizar Login:\n" + e.getLocalizedMessage().toString());
            alert.showAndWait();
        }

    }

//botão executa triagem JEF
    @FXML
    @SuppressWarnings("ObjectEqualsNull")
    private void handleNew(ActionEvent event) throws IOException {
        URL url = getClass().getResource("/SOUNDS/fanfare.wav");
        AudioClip clip = Applet.newAudioClip(url);
        clip.play();
        startTask();
    }
    
//botão executa triagem VF
    @FXML
    @SuppressWarnings("ObjectEqualsNull")
    private void handleNew2(ActionEvent event) throws IOException {
        URL url = getClass().getResource("/SOUNDS/have_this.wav");
        AudioClip clip = Applet.newAudioClip(url);
        clip.play();
        startTask2();
    }
//botão executa triagem EST
    @FXML
    @SuppressWarnings("ObjectEqualsNull")
    private void handleNew3(ActionEvent event) throws IOException {
        URL url = getClass().getResource("/SOUNDS/have_this.wav");
        AudioClip clip = Applet.newAudioClip(url);
        clip.play();
        startTask3();
    }
    
//botão retorna menu principal
    @FXML
   void RetornaMenu(ActionEvent event) {
        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/Root.fxml"));
        } catch (IOException ex) {

        }
        Scene scene = new Scene(root);
        stage.setMinWidth(400);
        stage.setMinHeight(400);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
        stage.setTitle("Mark-II");

    }


    public void runTask() {
        log.setText(log.getText() + "\n- Triagem Iniciada!");
        SpinnerJef.setVisible(true);
        doc = documento.isSelected();
        seq = sequencial.isSelected();
        trc = tc.isSelected();
        if (doc == false && seq == false && trc == false) {
            SpinnerJef.setVisible(false);
            URL url = getClass().getResource("/SOUNDS/error.wav");
            AudioClip clip = Applet.newAudioClip(url);
            clip.play();
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    erro.setTitle("Informação");
                    erro.setHeaderText("Codigo: X004");
                    erro.setContentText("Erro ao iniciar triagem:\n" + "Selecione previamente um modo de triagem");
                    erro.showAndWait();
                }
            });
        } else {
            try {
                //instaciamento da classe  para buscar o processo a ser triado 
                buscaprocesso find = new buscaprocesso(driver);
                boolean teste = false;
                teste = find.find(driver, antigo, doc, seq, trc/*, pericial*/);

                if (teste == false) {
                    SpinnerJef.setVisible(false);
                    URL url = getClass().getResource("/SOUNDS/special_item.wav");
                    AudioClip clip = Applet.newAudioClip(url);
                    clip.play();
                    JOptionPane.showMessageDialog(null, "Não há tarefas à serem triadas");
                    log.setText("- Triagem finalizada");

                } else if (teste == true) {
                    SpinnerJef.setVisible(false);
                    btnTest = false;
                    URL url = getClass().getResource("/SOUNDS/error.wav");
                    AudioClip clip = Applet.newAudioClip(url);
                    clip.play();
                    //       JOptionPane.showMessageDialog(null, "Erro de comunicação com plataforma Sapiens");
                    log.setText("- Erro de comunicação com plataforma Sapiens!");

                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                if (btnTest == true) {
                    SpinnerJef.setVisible(false);
                    btnTest = false;
                    URL url = getClass().getResource("/SOUNDS/error.wav");
                    AudioClip clip = Applet.newAudioClip(url);
                    clip.play();
                    //  JOptionPane.showMessageDialog(null, "Erro de comunicação com plataforma Sapiens");
                    log.setText("- Erro de comunicação com plataforma Sapiens!");

                } else {
                    SpinnerJef.setVisible(false);
                    URL url = getClass().getResource("/SOUNDS/error.wav");
                    AudioClip clip = Applet.newAudioClip(url);
                    clip.play();
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            erro.setTitle("Informação");
                            erro.setHeaderText("Codigo: X001");
                            erro.setContentText("Erro ao Realizar Triagem:\n" + "Faça o Login primeiro");
                            erro.showAndWait();
                        }
                    });
                    log.setText("- Erro: Login Não Realizado!");

                }

            }

        }

    }

    public void runTask2() {
        log.setText(log.getText() + "\n- Triagem Iniciada!");
        doc = documento.isSelected();
        seq = sequencial.isSelected();
        trc = tc.isSelected();
        if (doc == false && seq == false && trc == false) {
            SpinnerJef.setVisible(false);
            URL url = getClass().getResource("/SOUNDS/error.wav");
            AudioClip clip = Applet.newAudioClip(url);
            clip.play();
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    erro.setTitle("Informação");
                    erro.setHeaderText("Codigo: X004");
                    erro.setContentText("Erro ao iniciar triagem:\n" + "Selecione previamente um modo de triagem: ");
                    erro.showAndWait();
                }
            });
        } else {
            try {
                SpinnerVF.setVisible(true);
                log.setText(log.getText() + "\n- Triagem Iniciada!");
                //instaciamento da classe  para buscar o processo a ser triado 
                BuscaprocessoVF find = new BuscaprocessoVF(driver);
                boolean teste;
                teste = find.find(driver, antigoVF,doc,seq,trc);

                if (teste == false) {
                    SpinnerVF.setVisible(false);
                    URL url = getClass().getResource("/SOUNDS/special_item.wav");
                    AudioClip clip = Applet.newAudioClip(url);
                    clip.play();
                    JOptionPane.showMessageDialog(null, "Não há tarefas à serem triadas");
                    log.setText("- Triagem finalizada");
                } else if (teste == true) {
                    SpinnerVF.setVisible(false);
                    URL url = getClass().getResource("/SOUNDS/error.wav");
                    AudioClip clip = Applet.newAudioClip(url);
                    clip.play();
                    //JOptionPane.showMessageDialog(null, "Erro de comunicação com plataforma Sapiens");
                    log.setText("- Erro de comunicação com plataforma Sapiens!");
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                if (btnTest == true) {
                    SpinnerVF.setVisible(false);
                    URL url = getClass().getResource("/SOUNDS/error.wav");
                    AudioClip clip = Applet.newAudioClip(url);
                    clip.play();
                    //   JOptionPane.showMessageDialog(null, "Erro de comunicação com plataforma Sapiens");
                    log.setText("- Erro de comunicação com plataforma Sapiens!");
                } else {
                    SpinnerVF.setVisible(false);
                    URL url = getClass().getResource("/SOUNDS/error.wav");
                    AudioClip clip = Applet.newAudioClip(url);
                    clip.play();

                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            erro.setTitle("Informação");
                            erro.setHeaderText("Codigo: X001");
                            erro.setContentText("Erro ao Realizar Login:\n" + "Faça o Login primeiro");
                            erro.showAndWait();
                        }
                    });

                    //JOptionPane.showMessageDialog(null, "Faça o Login primeiro");
                    log.setText("- Erro: Login não realizado!");
                }
            }

        }
    }

    public void runTask3() {
/*
        try {
            Progress.setVisible(true);
            log.setText(log.getText() + "\n- Triagem Iniciada!");
            //instaciamento da classe para buscar o processo a ser triado 
            BuscaprocessoEst find = new BuscaprocessoEst(driver);
            boolean teste;
            teste = find.find(driver, antigoEst);

            if (teste == false) {
                Progress.setVisible(false);
                URL url = getClass().getResource("/SOUNDS/special_item.wav");
                AudioClip clip = Applet.newAudioClip(url);
                clip.play();
                JOptionPane.showMessageDialog(null, "Não há tarefas à serem triadas");
                log.setText("- Triagem finalizada");
            } else if (teste == true) {
                Progress.setVisible(false);
                URL url = getClass().getResource("/SOUNDS/error.wav");
                AudioClip clip = Applet.newAudioClip(url);
                clip.play();
                //JOptionPane.showMessageDialog(null, "Erro de comunicação com plataforma Sapiens");
                log.setText("- Erro de comunicação com plataforma Sapiens!");
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            if (btnTest == true) {
               Progress.setVisible(false);
                URL url = getClass().getResource("/SOUNDS/error.wav");
                AudioClip clip = Applet.newAudioClip(url);
                clip.play();
                //   JOptionPane.showMessageDialog(null, "Erro de comunicação com plataforma Sapiens");
                log.setText("- Erro de comunicação com plataforma Sapiens!");
            } else {
                Progress.setVisible(false);
                URL url = getClass().getResource("/SOUNDS/error.wav");
                AudioClip clip = Applet.newAudioClip(url);
                clip.play();

                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        erro.setTitle("Informação");
                        erro.setHeaderText("Codigo: X001");
                        erro.setContentText("Erro ao Realizar Login:\n" + "Faça o Login primeiro");
                        erro.showAndWait();
                    }
                });

                //JOptionPane.showMessageDialog(null, "Faça o Login primeiro");
                log.setText("- Erro: Login Não realizado!");
            }

        }
*/
        log.setText(log.getText() + "\n- Triagem Iniciada!");
        doc = documento.isSelected();
        seq = sequencial.isSelected();
        trc = tc.isSelected();
        if (doc == false && seq == false && trc == false) {
            SpinnerJef.setVisible(false);
            URL url = getClass().getResource("/SOUNDS/error.wav");
            AudioClip clip = Applet.newAudioClip(url);
            clip.play();
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    erro.setTitle("Informação");
                    erro.setHeaderText("Codigo: X004");
                    erro.setContentText("Erro ao iniciar triagem:\n" + "Selecione previamente um modo de triagem: ");
                    erro.showAndWait();
                }
            });
        } else {
            try {
                SpinnerEst.setVisible(true);
                log.setText(log.getText() + "\n- Triagem Iniciada!");
                //instaciamento da classe  para buscar o processo a ser triado 
                BuscaprocessoEst find = new BuscaprocessoEst(driver);
                boolean teste;
                teste = find.find(driver, antigoEst,doc,seq,trc);

                if (teste == false) {
                    SpinnerEst.setVisible(false);
                    URL url = getClass().getResource("/SOUNDS/special_item.wav");
                    AudioClip clip = Applet.newAudioClip(url);
                    clip.play();
                    JOptionPane.showMessageDialog(null, "Não há tarefas à serem triadas");
                    log.setText("- Triagem finalizada");
                } else if (teste == true) {
                    SpinnerEst.setVisible(false);
                    URL url = getClass().getResource("/SOUNDS/error.wav");
                    AudioClip clip = Applet.newAudioClip(url);
                    clip.play();
                    //JOptionPane.showMessageDialog(null, "Erro de comunicação com plataforma Sapiens");
                    log.setText("- Erro de comunicação com plataforma Sapiens!");
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                if (btnTest == true) {
                    SpinnerEst.setVisible(false);
                    URL url = getClass().getResource("/SOUNDS/error.wav");
                    AudioClip clip = Applet.newAudioClip(url);
                    clip.play();
                    //   JOptionPane.showMessageDialog(null, "Erro de comunicação com plataforma Sapiens");
                    log.setText("- Erro de comunicação com plataforma Sapiens!");
                } else {
                    SpinnerEst.setVisible(false);
                    URL url = getClass().getResource("/SOUNDS/error.wav");
                    AudioClip clip = Applet.newAudioClip(url);
                    clip.play();

                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            erro.setTitle("Informação");
                            erro.setHeaderText("Codigo: X001");
                            erro.setContentText("Erro ao Realizar Login:\n" + "Faça o Login primeiro");
                            erro.showAndWait();
                        }
                    });

                    //JOptionPane.showMessageDialog(null, "Faça o Login primeiro");
                    log.setText("- Erro: Login não realizado!");
                }
            }

        }
    }
    
    public void startTask() {
        // Create a Runnable
        Runnable task = new Runnable() {
            public void run() {
                runTask();
            }
        };

        // Run the task in a background thread
        Thread backgroundThread = new Thread(task);
        // Terminate the running thread if the application exits
        backgroundThread.setDaemon(true);
        // Start the thread
        backgroundThread.start();

    }

    public void startTask2() {
        // Create a Runnable
        Runnable task = new Runnable() {
            public void run() {
                runTask2();
            }
        };

        // Run the task in a background thread
        Thread backgroundThread = new Thread(task);
        // Terminate the running thread if the application exits
        backgroundThread.setDaemon(true);
        // Start the thread
        backgroundThread.start();
    }
    
     public void startTask3() {
        // Create a Runnable
        Runnable task = new Runnable() {
            public void run() {
                runTask3();
            }
        };

        // Run the task in a background thread
        Thread backgroundThread = new Thread(task);
        // Terminate the running thread if the application exits
        backgroundThread.setDaemon(true);
        // Start the thread
        backgroundThread.start();
    }

    @FXML
    public void Stop() {
        SpinnerVF.setVisible(false);
        SpinnerJef.setVisible(false);
        URL url = getClass().getResource("/SOUNDS/warp.wav");
        AudioClip clip = Applet.newAudioClip(url);
        clip.play();
        driver.quit();
    }

}
package com.mycompany.pje.federal;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javax.swing.JOptionPane;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class buscaprocesso
{

    public buscaprocesso(WebDriver driver)
    {
        super();
        // TODO Auto-generated constructor stub
    }

    //Encontra os processos
    @SuppressWarnings("SleepWhileInLoop")
    public final boolean find(WebDriver driver,boolean antigo,boolean doc,boolean seq,boolean tc/*, boolean pericial*/) throws InterruptedException, UnsupportedFlavorException, IOException, SQLException
    {
        URL url = getClass().getResource("/SOUNDS/ability-gain.wav");
        AudioClip clip = Applet.newAudioClip(url);

        Resultado resultado = new Resultado();
        /*
         * Foi necessário criar um delay entre a execução e o tempo de espera
         * para carregar a pagina web
         */
        //Thread.sleep(4000);
        //transforma a tabela num webelement manipulavel
        Thread.sleep(2000);
        //localiza a tabela onde estão armazenadas as tarefas
        // WebDriverWait wait = new WebDriverWait (driver,30);
        // wait.until(ExpectedConditions.presenceOfElementLocated(By.id("gridview-1104-body")));
        WebElement tabela = driver.findElement(By.id("gridview-1109-table"));
        //cria uma lista das tarefas na tela do usuário
        List<WebElement> tarefas = new ArrayList(tabela.findElements(By.cssSelector("tr")));
        /*
         * recebe o tamanho da lista, necessário para se realizar o laço e
         * executar a triagem sem a necessidade de intervenção do cliente
         */
        int tamanho = tarefas.size();
        // laço que executa a realização da triagem
        while (tamanho > 0)
        {
            // verifica a existencia da tabela e retorna lista vazia caso não haja tarefas
            try
            {
                // wait.until(ExpectedConditions.presenceOfElementLocated(By.id("gridview-1104-body")));
                Thread.sleep(2000);
                tabela = driver.findElement(By.id("gridview-1109-table"));
                tarefas = new ArrayList(tabela.findElements(By.cssSelector("tr")));
                // String Assunto = tarefas.get(0).getText(); inutilizado
                // MateriasPrev teste = new MateriasPrev(); inutilizado
//                if (true)
//                {

                    //clica no primeiro elemento da tabela
                    driver.findElement(By.xpath("//tr[1]/td[3]/div/a")).click();
                    try
                    {
                        Thread.sleep(2000);
                        //  cria uma lista com o endereçi de memoria das janelas abertas no navegador
                        List<String> janela = new ArrayList(driver.getWindowHandles());
                        //seleciona a janela da tarefa aberta
                        driver.switchTo().window(janela.get(1));
                        //instanciamento da classe para buscar arquivo dentro do processo
                        buscaarquivo buscaArquivo = new buscaarquivo();
                        //String checkCapa = buscaArquivo.Verifica_processo(driver);
                        // copia o texto do arquivo do processo e armazena numa string
//                 if (checkCapa.contains("MATÉRIA NÃO PREVIDÊNCIARIA"))
//                {
//                    resultado.setEtiqueta(checkCapa);
//                    resultado.setComplemetno("");
//                    resultado.setEtiqueta("");
//                }
//                else
//                {
                        int checkpoint = 1;
                        do
                        {
                            /* CÓDIGO ORIGINAL - REMOVIDO PARA TESTAR A BUSCA EXLUSIVA DE LAUDO PERICIAL
                            <!-------------------------------------------------------------------------/>
                            resultado = buscaArquivo.copia_processo(driver, checkpoint,antigo,doc,seq,tc,pericial);
                            if (resultado.getEtiqueta().contains("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO"))
                            {
                                checkpoint++;
                            }
                            else
                            {
                                break;
                            }*/
                            //Verifica se a opção de Busca Exclusiva de Laudo Pericial está ativa
                            //Caso a busca excusiva não esteja selecionada o Mark realiza o processo de triagem normalmente
                            /*if(pericial==true) {
                                resultado = buscaArquivo.pericial(driver);
                                break; //Break utilizado para que ESTE metedo não chame buscaArquivo.pericial() três vezes
                            } else { 
                                resultado = buscaArquivo.copia_processo(driver, checkpoint,antigo,doc,seq,tc,pericial);
                            }*/
                            resultado = buscaArquivo.copia_processo(driver, checkpoint,antigo,doc,seq,tc/*,pericial*/);
                            if (resultado.getEtiqueta().contains("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO")){
                                    //|| resultado.getEtiqueta().contains("NÃO FOI POSSÍVEL LOCALIZAR LAUDO PERICIAL")) {
                                checkpoint++;
                            }
                            else
                            {
                                break;
                            }
                            
                        } while (checkpoint < 3);
                        //  }
                        String Local = resultado.getLocal();
                        String processo = resultado.getEtiqueta();
                        String radical = resultado.getRadical();
                        String complemento = resultado.getComplemetno();
                        // compara palavra chave
                        // fecha a segunda janela e volta para primeira janela
                        driver.switchTo().window(janela.get(1)).close();
                        driver.switchTo().window(janela.get(0));
                        Thread.sleep(2000);
                        // Modulo para inserir etiqueta
                        Etiqueta etiqueta1 = new Etiqueta();
                        //etiqueta1.exec_etiqueta(driver, etiquetinha);
                        etiqueta1.exec_etiqueta(driver, processo, radical, complemento, Local);
                    }
                    catch (Exception e)
                    {
                        
                    final Exception ex = e;

                    Platform.runLater(new Runnable()
                    {

                        @Override
                        public void run()
                        {
                            Alert erro = new Alert(Alert.AlertType.WARNING);
                            erro.setTitle("Informação");
                            erro.setHeaderText("Codigo: X002");
                            erro.setContentText("Erro no processo de Triagem:\n");
                            StringWriter sw = new StringWriter();
                            PrintWriter pw = new PrintWriter(sw);
                            ex.printStackTrace(pw);
                            String exceptionText = sw.toString();

                            Label label = new Label("The exception stacktrace was:");

                            TextArea textArea = new TextArea(exceptionText);
                            textArea.setEditable(false);
                            textArea.setWrapText(true);

                            textArea.setMaxWidth(Double.MAX_VALUE);
                            textArea.setMaxHeight(Double.MAX_VALUE);
                            GridPane.setVgrow(textArea, Priority.ALWAYS);
                            GridPane.setHgrow(textArea, Priority.ALWAYS);

                            GridPane expContent = new GridPane();
                            expContent.setMaxWidth(Double.MAX_VALUE);
                            expContent.add(label, 0, 0);
                            expContent.add(textArea, 0, 1);

                            // Set expandable Exception into the dialog pane.
                            erro.getDialogPane().setExpandableContent(expContent);
                            erro.showAndWait();
                        }
                    });
                        return true;
                    }
//                }
//                else
//                {
//                    Etiqueta etiqueta1 = new Etiqueta();
//                    etiqueta1.exec_etiqueta(driver, "MATERIA NÃO PREVIDENCIARIA", "", "");
//                }

            }
            catch (Exception e)
            {
                clip.play();
                int op = JOptionPane.showConfirmDialog(null, "Demora no tempo de resposta do Sapiens\nDeseja continuar triando?", "Triagem encerrada", JOptionPane.YES_NO_OPTION);
                if (op == 0)
                {
                    Thread.sleep(2000);
                }
                else
                {
                    return false;
                }
            }

        }
        return false;
    }
}

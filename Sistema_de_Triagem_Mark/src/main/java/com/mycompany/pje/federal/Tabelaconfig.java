/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carlo
 */
public class Tabelaconfig {

    public List<Headers> header() {
        List<Headers> cabecalho = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:Config.db");
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM header ORDER BY headerDoc ");
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                Headers key = new Headers();
                key.setAssunto(resultSet.getString("headerDoc"));
                cabecalho.add(key);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Tabelaconfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cabecalho;
    }

    public List<ProvJuri> ProJuri() {
        List<ProvJuri> prov = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:Config.db");
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM ProvJuri ORDER BY providencia");
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                ProvJuri key = new ProvJuri();
                key.setProvidencia(resultSet.getString("providencia"));
                prov.add(key);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Tabelaconfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        return prov;
    }

    public List<Execao> execao() {
        List<Execao> prov = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:Config.db");
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Execao ORDER BY execao");
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                Execao key = new Execao();
                key.setEx(resultSet.getString("execao"));
                prov.add(key);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Tabelaconfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        return prov;
    }
}

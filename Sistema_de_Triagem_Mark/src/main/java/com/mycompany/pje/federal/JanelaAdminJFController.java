/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author root
 */
public class JanelaAdminJFController implements Initializable {

    @FXML
    RadioButton LerMov;
    @FXML
    RadioButton LerDoc;
    @FXML
    JFXTextField Radical;
    @FXML
    JFXTextField Complemento;
    @FXML
    JFXTextField Etiqueta;
    @FXML
    TableView<Chaves> tablebank;
    @FXML
    TableColumn<Chaves, String> colRadical;
    @FXML
    TableColumn<Chaves, String> colComplemento;
    @FXML
    TableColumn<Chaves, String> colEtiqueta;
    @FXML
    TableColumn<Chaves, String> colPeso;
    @FXML
    TableColumn<Chaves, String> Indice;
    @FXML
    JFXTextField pesquisa;
    @FXML
    JFXTextField N_etiquetas;
    @FXML
    RadioButton P1;
    @FXML
    RadioButton P2;
    @FXML
    RadioButton P3;
    @FXML
    RadioButton P4;

    final ToggleGroup peso = new ToggleGroup();

    final ToggleGroup group = new ToggleGroup();

    @FXML
    void RetornaMenu(ActionEvent event) {
        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/Root.fxml"));
        } catch (IOException ex) {

        }
        Scene scene = new Scene(root);
        stage.setMinWidth(400);
        stage.setMinHeight(400);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();
        stage.setTitle("Mark-II");
    }

    @FXML
    @SuppressWarnings("ConvertToTryWithResources")
    private boolean inserir() {
        String radical = Radical.getText().toUpperCase();
        String complemento = Complemento.getText().toUpperCase();
        String etiqueta = Etiqueta.getText().toUpperCase();
        Banco banco = new Banco();
        Chaves chave = new Chaves();
        if ((radical.equals(null)) || complemento.equals(null) || etiqueta.equals(null)) {
            JOptionPane.showMessageDialog(null, "Erro: Não insira palavras em branco");
            return false;
        } else if ((radical.equals("")) || complemento.equals("") || etiqueta.equals("")) {
            JOptionPane.showMessageDialog(null, "Erro: Não insira palavras em branco");
            return false;
        } else if ((radical.equals(" ")) || complemento.equals(" ") || etiqueta.equals(" ")) {
            JOptionPane.showMessageDialog(null, "Erro: Não insira palavras em branco");
            return false;
        } else {
            chave.setRadical(Radical.getText().toUpperCase().trim().replace("'", "").replace("´", ""));
            chave.setComplemento(Complemento.getText().toUpperCase().trim().replace("'", "").replace("´", ""));
            chave.setEtiqueta(Etiqueta.getText().toUpperCase().trim().replace("'", "").replace("´", ""));
            chave.setPrioridade(Peso());
            if (LerMov.isSelected()) {
                chave.setTipo("TABELA");
            } else {
                chave.setTipo("DOC");
            }
            Radical.setText(null);
            Complemento.setText(null);
            Etiqueta.setText(null);
            banco.inserirVF(chave);
        }
        try {

            List<Chaves> chaves = new ArrayList<>();
            Connection connection = DriverManager.getConnection("jdbc:sqlite:bancoJF.db");
            // lendo os registros
            PreparedStatement stmt = connection.prepareStatement("select * from triagem order by RADICAL ");
            ResultSet resultSet = stmt.executeQuery();
            int i = 1;
            while (resultSet.next()) {
                Chaves key = new Chaves();
                key.setRadical(resultSet.getString("RADICAL"));
                key.setComplemento(resultSet.getString("COMPLEMENTO"));
                key.setEtiqueta(resultSet.getString("ETIQUETA"));
                key.setTipo(resultSet.getString("TIPO"));
                key.setPrioridade(resultSet.getString("PRIORIDADE"));
                chaves.add(key);
                i++;
            }

            colRadical.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Radical"));
            colComplemento.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Complemento"));
            colEtiqueta.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Etiqueta"));
            colPeso.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Prioridade"));
            Indice.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Tipo"));
            ObservableList<Chaves> genericos = FXCollections.observableArrayList(chaves);
            N_etiquetas.setText(i + "");
            tablebank.setItems(genericos);
            connection.close();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return true;
    }

    @FXML
    public void busca() throws SQLException {
        List<Chaves> chaves = new ArrayList<>();
        Connection connection = DriverManager.getConnection("jdbc:sqlite:bancoJF.db");
        // lendo os registros
        PreparedStatement stmt = connection.prepareStatement("select * from triagem where ETIQUETA like '%" + pesquisa.getText().toUpperCase().trim().replace("'", "").replace("´", "") + "%' "
                + "OR RADICAL like '%" + pesquisa.getText().toUpperCase().trim().replace("'", "").replace("´", "") + "%' "
                + "OR COMPLEMENTO like '%" + pesquisa.getText().toUpperCase().trim().replace("'", "").replace("´", "") + "%' "
                + "OR TIPO like '%" + pesquisa.getText().toUpperCase().trim().replace("'", "").replace("´", "") + "%' ");
        ResultSet resultSet = stmt.executeQuery();
        int i = 1;
        while (resultSet.next()) {
            Chaves key = new Chaves();
            key.setRadical(resultSet.getString("RADICAL"));
            key.setComplemento(resultSet.getString("COMPLEMENTO"));
            key.setEtiqueta(resultSet.getString("ETIQUETA"));
            key.setPrioridade(resultSet.getString("PRIORIDADE"));
            key.setTipo(resultSet.getString("TIPO"));
            chaves.add(key);
            i++;
        }

        colRadical.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Radical"));
        colComplemento.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Complemento"));
        colEtiqueta.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Etiqueta"));
        colPeso.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Prioridade"));
        Indice.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Tipo"));
        ObservableList<Chaves> genericos = FXCollections.observableArrayList(chaves);
        N_etiquetas.setText(i + "");
        tablebank.setItems(genericos);
        connection.close();

    }

    @FXML
    @SuppressWarnings("ConvertToTryWithResources")
    public void excluir() {
        int op;
        op = JOptionPane.showConfirmDialog(null, "Deseja Realmente Apagar este Registro?", "Apagar Registro", JOptionPane.YES_NO_OPTION);
        if (op == JOptionPane.YES_OPTION) {
            Banco banco = new Banco();
            Chaves chave = new Chaves();
            chave.setRadical(Radical.getText());
            chave.setComplemento(Complemento.getText());
            //chave.setRadical(Radical.getText().trim().replace("'","").replace("´", ""));
            //chave.setComplemento(Complemento.getText().trim().replace("'","").replace("´", ""));
            banco.excluir_citacao(chave);
            Radical.clear();
            Complemento.clear();
            Etiqueta.clear();
            try {

                List<Chaves> chaves = new ArrayList<>();
                Connection connection = DriverManager.getConnection("jdbc:sqlite:bancoJF.db");
                // lendo os registros
                PreparedStatement stmt = connection.prepareStatement("select * from triagem order by RADICAL ");
                ResultSet resultSet = stmt.executeQuery();
                int i = 1;
                while (resultSet.next()) {
                    Chaves key = new Chaves();
                    key.setRadical(resultSet.getString("RADICAL"));
                    key.setComplemento(resultSet.getString("COMPLEMENTO"));
                    key.setEtiqueta(resultSet.getString("ETIQUETA"));
                    key.setTipo(resultSet.getString("TIPO"));
                    key.setPrioridade(resultSet.getString("PRIORIDADE"));
                    chaves.add(key);
                    i++;
                }

                colRadical.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Radical"));
                colComplemento.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Complemento"));
                colEtiqueta.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Etiqueta"));
                colPeso.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Prioridade"));
                Indice.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Tipo"));
                ObservableList<Chaves> genericos = FXCollections.observableArrayList(chaves);
                N_etiquetas.setText(i + "");
                tablebank.setItems(genericos);
                connection.close();

            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        } else {

            Radical.clear();
            Complemento.clear();
            Etiqueta.clear();

        }
    }

    @SuppressWarnings("ConvertToTryWithResources")
    public void atualizar() {
        try {

            List<Chaves> chaves = new ArrayList<>();
            Connection connection = DriverManager.getConnection("jdbc:sqlite:bancoJF.db");
            // lendo os registros
            PreparedStatement stmt = connection.prepareStatement("select * from triagem order by RADICAL ");
            ResultSet resultSet = stmt.executeQuery();
            int i = 1;
            while (resultSet.next()) {
                Chaves key = new Chaves();
                key.setRadical(resultSet.getString("RADICAL"));
                key.setComplemento(resultSet.getString("COMPLEMENTO"));
                key.setEtiqueta(resultSet.getString("ETIQUETA"));
                key.setPrioridade(resultSet.getString("PRIORIDADE"));
                key.setTipo(resultSet.getString("TIPO"));
                chaves.add(key);
                i++;
            }

            colRadical.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Radical"));
            colComplemento.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Complemento"));
            colEtiqueta.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Etiqueta"));
            colPeso.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Prioridade"));
            Indice.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Tipo"));
            ObservableList<Chaves> genericos = FXCollections.observableArrayList(chaves);
            tablebank.setItems(genericos);
            N_etiquetas.setText(i + "");

            connection.close();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public void alterar() throws IOException {
        Chaves chave = new Chaves();
        chave.setRadical(tablebank.getSelectionModel().getSelectedItem().getRadical().replace("'", "").replace("´", ""));
        chave.setComplemento(tablebank.getSelectionModel().getSelectedItem().getComplemento().replace("'", "").replace("´", ""));
        chave.setEtiqueta(tablebank.getSelectionModel().getSelectedItem().getEtiqueta().replace("'", "").replace("´", ""));
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/EditVF.fxml"));
        loader.setController(new EditController(chave));
        Parent root = loader.load();
        Stage stage = new Stage();
        stage.setTitle("Editar Chaves");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(new Scene(root));
        stage.show();
    }

    public void limpar() {

        Radical.clear();
        Complemento.clear();
        Etiqueta.clear();

    }

    public void selecionar() {

        Radical.clear();
        Complemento.clear();
        Etiqueta.clear();
        Radical.setText(tablebank.getSelectionModel().getSelectedItem().getRadical());
        Complemento.setText(tablebank.getSelectionModel().getSelectedItem().getComplemento());
        Etiqueta.setText(tablebank.getSelectionModel().getSelectedItem().getEtiqueta());

    }

    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void initialize(URL location, ResourceBundle resources) {
        //Inicializar tabela 1
        try {

            List<Chaves> chaves = new ArrayList<>();
            Connection connection = DriverManager.getConnection("jdbc:sqlite:bancoJF.db");
            // lendo os registros
            PreparedStatement stmt = connection.prepareStatement("select * from triagem order by RADICAL ");
            ResultSet resultSet = stmt.executeQuery();
            int i = 1;
            while (resultSet.next()) {
                Chaves key = new Chaves();
                key.setRadical(resultSet.getString("RADICAL"));
                key.setComplemento(resultSet.getString("COMPLEMENTO"));
                key.setEtiqueta(resultSet.getString("ETIQUETA"));
                key.setTipo(resultSet.getString("TIPO"));
                key.setPrioridade(resultSet.getString("PRIORIDADE"));
                chaves.add(key);
                i++;
            }

            colRadical.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Radical"));
            colComplemento.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Complemento"));
            colEtiqueta.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Etiqueta"));
            colPeso.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Prioridade"));
            Indice.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Tipo"));
            ObservableList<Chaves> genericos = FXCollections.observableArrayList(chaves);
            N_etiquetas.setText(i + "");

            tablebank.setItems(genericos);
            connection.close();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public String Peso() {
        if (P1.isSelected()) {
            return "1";
        } else if (P2.isSelected()) {
            return "2";
        } else if (P3.isSelected()) {
            return "3";
        } else if (P4.isSelected()) {
            return "4";
        }
        return null;
    }//Alterado
}
package com.mycompany.pje.federal;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.swing.JOptionPane;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class BuscaprocessoVF
{

    public BuscaprocessoVF(WebDriver driver)
    {
        super();

    }

    @SuppressWarnings("SleepWhileInLoop")
    public final boolean find(WebDriver driver, boolean antigoVF,boolean doc,boolean seq,boolean tc) throws InterruptedException, UnsupportedFlavorException, IOException, SQLException
    {
        URL url = getClass().getResource("/SOUNDS/ability-gain.wav");
        AudioClip clip = Applet.newAudioClip(url);
        Resultado resultado = new Resultado();
        Thread.sleep(2000);
        WebElement tabela = driver.findElement(By.id("gridview-1109-table"));
        //Cria uma lista das tarefas na tela do usuário
        List<WebElement> tarefas = new ArrayList(tabela.findElements(By.cssSelector("tr")));
        int tamanho = tarefas.size();
        while (tamanho > 0)
        {
            //Verifica a existencia da tabela e retorna lista vazia caso não haja tarefas
            try
            {
                Thread.sleep(2000);
                tabela = driver.findElement(By.id("gridview-1109-table"));
                tarefas = new ArrayList(tabela.findElements(By.cssSelector("tr")));
                driver.findElement(By.xpath("//tr[1]/td[3]/div/a")).click();
                try
                {
                    Thread.sleep(2000);
                    //Cria uma lista com o endereçi de memoria das janelas abertas no navegador
                    List<String> janela = new ArrayList(driver.getWindowHandles());
                    //Seleciona a janela da tarefa aberta
                    driver.switchTo().window(janela.get(1));
                    //Instanciamento da classe para buscar arquivo dentro do processo
                    BuscaArquivoVF buscaArquivo = new BuscaArquivoVF();
                    int checkpoint = 1;
                    do
                    {
                        resultado = buscaArquivo.execVF(driver, checkpoint, antigoVF, doc, seq, tc);
                        if (resultado.getEtiqueta().contains("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO"))
                        {
                            checkpoint++;
                        }
                        else
                        {
                            break;
                        }
                    } while (checkpoint < 3);
                    String Local = resultado.getLocal();
                    String processo = resultado.getEtiqueta();
                    String radical = resultado.getRadical();
                    String complemento = resultado.getComplemetno();
                    // compara palavra chave
                    // fecha a segunda janela e volta para primeira janela
                    driver.switchTo().window(janela.get(1)).close();
                    driver.switchTo().window(janela.get(0));
                    Thread.sleep(2000);
                    // Modulo para inserir etiqueta
                    Etiqueta etiqueta1 = new Etiqueta();
                    //etiqueta1.exec_etiqueta(driver, etiquetinha);
                    etiqueta1.exec_etiqueta(driver, processo, radical, complemento, Local);

                }
                catch (Exception e)
                {
                    driver.quit();
                    final Exception ex = e;

                    Platform.runLater(new Runnable()
                    {

                        @Override
                        public void run()
                        {
                            Alert erro = new Alert(AlertType.WARNING);
                            erro.setTitle("Informação");
                            erro.setHeaderText("Codigo: X003");
                            erro.setContentText("Erro no processo de Triagem:\n");
                            StringWriter sw = new StringWriter();
                            PrintWriter pw = new PrintWriter(sw);
                            ex.printStackTrace(pw);
                            String exceptionText = sw.toString();

                            Label label = new Label("The exception stacktrace was:");

                            TextArea textArea = new TextArea(exceptionText);
                            textArea.setEditable(false);
                            textArea.setWrapText(true);

                            textArea.setMaxWidth(Double.MAX_VALUE);
                            textArea.setMaxHeight(Double.MAX_VALUE);
                            GridPane.setVgrow(textArea, Priority.ALWAYS);
                            GridPane.setHgrow(textArea, Priority.ALWAYS);

                            GridPane expContent = new GridPane();
                            expContent.setMaxWidth(Double.MAX_VALUE);
                            expContent.add(label, 0, 0);
                            expContent.add(textArea, 0, 1);

                            erro.getDialogPane().setExpandableContent(expContent);
                            erro.showAndWait();
                        }
                    });
                    return true;
                }
            }
            catch (Exception e)
            {
                clip.play();
                int op = JOptionPane.showConfirmDialog(null, "Demora no tempo de resposta do Sapiens\nDeseja continuar triando?", "Triagem encerrada", JOptionPane.YES_NO_OPTION);
                if (op == 0)
                {
                    Thread.sleep(2000);
                }
                else
                {
                    return false;
                }
            }
        }
        return false;
    }
}
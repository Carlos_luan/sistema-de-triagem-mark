package com.mycompany.pje.federal;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.sql.SQLException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class BuscaarquivoEst
{

    public void sobe(WebDriver driver)
    {
        Actions action = new Actions(driver);
        action.sendKeys(Keys.PAGE_UP).build().perform();
        action.sendKeys(Keys.PAGE_UP).build().perform();
    }
   
    public Resultado copia_processo(WebDriver driver, int checkpoint,boolean antigo,boolean doc,boolean seq,boolean tc) throws InterruptedException, SQLException, UnsupportedFlavorException, IOException
    {
        Resultado resultado = new Resultado();
        
        ClicaProcessoPJEEstadual click = new ClicaProcessoPJEEstadual();
        String teste = "";
        boolean check;
        //Verifica se a movimentação está pronta
        click.ClicaProcessoPJEEstadual(driver);
        Tratamento tratamento = new Tratamento();
        if (checkpoint == 1 && (seq == true || tc == true)) {
            resultado = click.ExecTabelaEst(driver, resultado,antigo);
            if (!resultado.getEtiqueta().contains("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO")) {
                resultado.setLocal("Movimentação");
                return resultado;
            }
            check = resultado.getCheck();
        } else if (doc == true || tc == true) {
            check = click.executarEst(driver, antigo);
            if (!check) {
                resultado.setLocal("Documento");
                resultado.setEtiqueta("NÃO FOI POSSÍVEL LOCALIZAR ATO JUDICIAL ATUALIZADO (EST)");
                resultado.setRadical("");
                resultado.setComplemetno("");
                return resultado;
            } else {
                //Simula Ctrl+A / Ctrl+C (COPIAR)
                Actions action = new Actions(driver);
                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0061')).perform();
                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0063')).perform();
                Thread.sleep(2000);
                //Colando documento do clipboard para uma string / Ctrl+V (COLAR)
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                DataFlavor flavor = DataFlavor.stringFlavor;
                
                teste = clipboard.getData(flavor).toString();
                if (teste.length() > 1)
                {
                    try
                    {
                        String processo = "";
                        String etiquetinha = "";
                        processo = clipboard.getData(flavor).toString();
                        processo = tratamento.removerAcento(processo);
                        Triagem busca = new Triagem();
                        //recebe a etiqueta que foi identificada na triagem.
                        resultado = busca.TriarEst(/*driver, */processo, "DOC");
                        etiquetinha = resultado.getEtiqueta();
                        if (etiquetinha.contains("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO"));
                        {
                            for (int i = 0; i < 3; i++)
                            {
                                WebElement Tela2 = driver.findElement(By.xpath("//*[@id=\"myiframe-body\"]"));
                                Tela2.click();
                                sobe(driver);
                                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0061')).perform();
                                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0063')).perform();
                                action.keyUp(Keys.CONTROL).build().perform();
                                Thread.sleep(2000);
                                clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                                flavor = DataFlavor.stringFlavor;
                                processo = clipboard.getData(flavor).toString();
                                processo = processo.toUpperCase();
                                processo = tratamento.removerAcento(processo);
                                resultado = busca.TriarEst(/*driver, */processo, "DOC");
                                etiquetinha = resultado.getEtiqueta();
                                System.out.println("\n \t" + processo);
                                if (!etiquetinha.contains("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO"))
                                {
                                    return resultado;
                                }
                            }
                        }

                    }
                    catch (UnsupportedFlavorException e)
                    {
                        System.out.println(e);
                    }
                    catch (IOException e)
                    {
                        System.out.println(e);
                    }
                }
                else
                {
                    resultado.setEtiqueta("ERRO EM TRIAGEM: PDF NÃO PESQUISÁVEL");
                    return resultado;
                }
            }
        }
        return resultado;
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.swing.JOptionPane;

/**
 *
 * @author root2
 */

public class AssuntoPrevController implements Initializable {

    @FXML
    TableView<Headers> tablePrev;
    @FXML
    TableColumn<Headers, String> colAssunto;
    @FXML
    JFXTextField AssuntoTxt;
    Tabelaconfig refresh = new Tabelaconfig();
    // create a text input dialog 
    TextInputDialog td = new TextInputDialog();
    @FXML
    TableView<ProvJuri> Pjuri;
    @FXML
    TableColumn<ProvJuri, String> ColPjuri;
    @FXML
    TableView<Execao> Exe;
    @FXML
    TableColumn<Execao, String> ColExe;
    @FXML
    JFXTextField ProvJuriTxt;
    @FXML
    JFXTextField ExeTxt;

    @FXML
    public void inserirProv() {
        ProvJuri elemento = new ProvJuri();
        String radical = ProvJuriTxt.getText();
        if ((radical.equals(null))) {
            JOptionPane.showMessageDialog(null, "Erro: Não insira palavras em branco");
        } else {
            elemento.setProvidencia(radical);
            inserir("ProvJuri", elemento.getProvidencia());
            ColPjuri.setCellValueFactory(new PropertyValueFactory<ProvJuri, String>("Providencia"));
            ObservableList<ProvJuri> pj = FXCollections.observableArrayList(refresh.ProJuri());
            Pjuri.setItems(pj);
            ProvJuriTxt.clear();
        }
    }

    @FXML
    public void inserirExe() {
        Execao elemento = new Execao();
        String radical = ExeTxt.getText();
        if ((radical.equals(null))) {
            JOptionPane.showMessageDialog(null, "Erro: Não insira palavras em branco");
        } else {
            elemento.setEx(radical);
            inserir("Execao", elemento.getEx());
            ColExe.setCellValueFactory(new PropertyValueFactory<Execao, String>("ex"));
            ObservableList<Execao> ex = FXCollections.observableArrayList(refresh.execao());
            Exe.setItems(ex);
            ExeTxt.clear();
        }
    }

    @FXML
    public void inserirHead() {
        Headers chave = new Headers();
        String radical = AssuntoTxt.getText().toUpperCase();
        if ((radical.equals(null))) {
            JOptionPane.showMessageDialog(null, "Erro: Não insira palavras em branco");
        } else {
            chave.setAssunto(radical);
            inserir("header", chave.getAssunto());
            List<Headers> cabecalho = new ArrayList<>();
            colAssunto.setCellValueFactory(
                    new PropertyValueFactory<Headers, String>(
                            "Assunto"));
            ObservableList<Headers> genericos = FXCollections.observableArrayList(refresh.header());
            tablePrev.setItems(genericos);
            AssuntoTxt.clear();
        }
    }

    @FXML
    public void excluirHead() {
        excluir(tablePrev.getSelectionModel().getSelectedItem().getAssunto(), "header", "headerDoc");
        colAssunto.setCellValueFactory(new PropertyValueFactory<Headers, String>("Assunto"));
        ObservableList<Headers> genericos = FXCollections.observableArrayList(refresh.header());
        tablePrev.setItems(genericos);
    }

    @FXML
    public void excluirProv() {
        excluir(Pjuri.getSelectionModel().getSelectedItem().getProvidencia(), "ProvJuri", "providencia");
        ColPjuri.setCellValueFactory(new PropertyValueFactory<ProvJuri, String>("Providencia"));
        ObservableList<ProvJuri> genericos = FXCollections.observableArrayList(refresh.ProJuri());
        Pjuri.setItems(genericos);
    }

    @FXML
    public void excluirEx() {
        excluir(Exe.getSelectionModel().getSelectedItem().getEx(), "Execao", "execao");
        ColExe.setCellValueFactory(new PropertyValueFactory<Execao, String>("ex"));
        ObservableList<Execao> genericos = FXCollections.observableArrayList(refresh.execao());
        Exe.setItems(genericos);
    }

    public void excluir(String txt, String local, String coluna) {
        int op;
        op = JOptionPane.showConfirmDialog(null, "Deseja Realmente Apagar este Registro?", "Apagar Registro", JOptionPane.YES_NO_OPTION);
        if (op == JOptionPane.YES_OPTION) {
            Banco banco = new Banco();
            Headers chave = new Headers();
            chave.setAssunto(txt);
            AssuntoTxt.clear();
            banco.excluirConfig(chave, local, coluna);
        }
    }

    @FXML
    public void handle() {

        // show the text input dialog 
        //td.showAndWait();
        Optional<String> result = td.showAndWait();
        if (result.isPresent()) {
            Headers headers = new Headers();
            headers.setAssunto(result.get().toUpperCase());
            Banco banco = new Banco();
            banco.alterarConfig(headers, tablePrev.getSelectionModel().getSelectedItem().getAssunto());
            List<Headers> cabecalho = new ArrayList<>();

            colAssunto.setCellValueFactory(new PropertyValueFactory<Headers, String>("Assunto"));
            ObservableList<Headers> genericos = FXCollections.observableArrayList(refresh.header());
            tablePrev.setItems(genericos);

        }
    }

    @FXML
    public void limpar() {
        AssuntoTxt.clear();
        ProvJuriTxt.clear();
        ExeTxt.clear();
    }

    @FXML
    void RetornaMenu(ActionEvent event
    ) {
        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/Root.fxml"));
        } catch (IOException ex) {

        }
        Scene scene = new Scene(root);
        stage.setMinWidth(400);
        stage.setMinHeight(400);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
        stage.setTitle("Mark-II");

    }

    @FXML
    void help(ActionEvent event
    ) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Informação");
        alert.setHeaderText("Cadastro de Cabeçalho padrão do documento!");
        alert.setContentText("Nesta janela você deverá cadastrar o cabeçalho padrão das páginas que o Sistema de Triagem Mark precisa ler!"
                + "\nExemplo: VARA FEDERAL DE JUIZADO ESPECIAL CÍVEL");
        alert.showAndWait();
    }

    private boolean inserir(String local, String txt) {
        Banco banco = new Banco();
        Headers chave = new Headers();
        chave.setAssunto(txt);
        banco.inserirConfig(chave, local);
        return true;
    }

    @FXML
    public void selecionar() {
        AssuntoTxt.clear();
        AssuntoTxt.setText(tablePrev.getSelectionModel().getSelectedItem().getAssunto());
    }

    @FXML
    public void selecionarProv() {
        ProvJuriTxt.clear();
        ProvJuriTxt.setText(Pjuri.getSelectionModel().getSelectedItem().getProvidencia());
    }

    @FXML
    public void selecionarEx() {
        ExeTxt.clear();
        ExeTxt.setText(Exe.getSelectionModel().getSelectedItem().getEx());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        /*
        inicialização da tabela de cabeçalho
         */
        colAssunto.setCellValueFactory(new PropertyValueFactory<Headers, String>("Assunto"));
        ObservableList<Headers> genericos = FXCollections.observableArrayList(refresh.header());
        tablePrev.setItems(genericos);
        /*
        inicialização da tabela de providencia juridica
         */
        ColPjuri.setCellValueFactory(new PropertyValueFactory<ProvJuri, String>("Providencia"));
        ObservableList<ProvJuri> pj = FXCollections.observableArrayList(refresh.ProJuri());
        Pjuri.setItems(pj);
        /*
        inicialização da tabela de Exeção
         */
        ColExe.setCellValueFactory(new PropertyValueFactory<Execao, String>("ex"));
        ObservableList<Execao> ex = FXCollections.observableArrayList(refresh.execao());
        Exe.setItems(ex);

        td.setTitle("Editar Registro");
        td.setHeaderText("Insira a alteração do registro");
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class buscaarquivo {

    public void sobe(WebDriver driver) {
        Actions action = new Actions(driver);
        action.sendKeys(Keys.PAGE_UP).build().perform();
        action.sendKeys(Keys.PAGE_UP).build().perform();
        action.sendKeys(Keys.PAGE_UP).build().perform();
    }
    //Alterado em 19/10/2020
    public void desce(WebDriver driver) {
        Actions action = new Actions(driver);
        //action.sendKeys(Keys.END).build().perform();
        //action.sendKeys(Keys.PAGE_DOWN).build().perform();                        
    }
    //Alterado em 19/10/2020
    public Resultado copia_processo(WebDriver driver, int checkpoint, boolean antigo, boolean doc, boolean seq, boolean tc/*, boolean pericial*/) throws InterruptedException, SQLException, UnsupportedFlavorException, IOException {
        Resultado resultado = new Resultado();
        ClicaProcesso click = new ClicaProcesso();
        Triagem busca = new Triagem();
        String teste = "";
        boolean check;
        
        //Verifico se a movimentação está pronta
        click.ClicaProcesso(driver);
        Tratamento tratamento = new Tratamento();

        if (checkpoint == 1 && (seq == true || tc == true)) {
            resultado = click.ExecTabela(driver, resultado, antigo);
            if (!resultado.getEtiqueta().contains("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO")) {
                resultado.setLocal("Movimentação");
                return resultado;
            }
            check = resultado.getCheck();
        } else if (doc == true || tc == true) {
            check = click.executar(driver, antigo);
            if (!check) {
                resultado.setLocal("Documento");
                resultado.setEtiqueta("NÃO FOI POSSÍVEL LOCALIZAR ATO JUDICIAL ATUALIZADO (JEF)");
                resultado.setRadical("");
                resultado.setComplemetno("");
                return resultado;
            } else {
                //Simula Ctrl+A (SELECIONA TODOS) / Ctrl+C (COPIAR)
                Actions action = new Actions(driver);
                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0061')).perform();
                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0063')).perform();
                Thread.sleep(2000);
                /*
                //Colando documento do clipboard para uma string (CTRL+V) (COLAR)
                StringSelection stringSelection = new StringSelection("");
                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
                */
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                DataFlavor flavor = DataFlavor.stringFlavor;
                
                teste = clipboard.getData(flavor).toString();
                
                //Se existirem dados dentro do processo ele executa
                if (teste.length() > 1) {
                    try {
                        
                        String etiquetinha = "";
                        /*
                        clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                        flavor = DataFlavor.stringFlavor;
                        teste = clipboard.getData(flavor).toString();*/
                        //Necessário encontrar a metade da string e soma-la para andar adiante do vetor
                        //teste = tratamento.removerAcento(teste);

                        //Recebe a etiqueta que foi identificada na triagem.
                        resultado = busca.find_JEF(teste, "DOC");
                        etiquetinha = resultado.getEtiqueta();
   
                        if (etiquetinha.contains("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO")) {
                            for (int a = 0; a < 3; a++) {
                                WebElement Tela2 = driver.findElement(By.xpath("//*[@id=\"myiframe-body\"]"));
                                Tela2.click();
                                sobe(driver);
                                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0061')).perform();
                                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0063')).perform();
                                action.keyUp(Keys.CONTROL).build().perform();
                                Thread.sleep(2000);
                                /* Lib StringSelection pode estar causando erro no processo de triagem
                                stringSelection = new StringSelection("");
                                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
                                */
                                clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                                flavor = DataFlavor.stringFlavor;

                                teste = clipboard.getData(flavor).toString();
                                teste = teste.toUpperCase();
                                teste = tratamento.removerAcento(teste);

                                resultado = busca.find_JEF(teste, "DOC");
                                etiquetinha = resultado.getEtiqueta();
                                if (!etiquetinha.contains("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO")) {
                                    return resultado;
                                }
                            }
                        }

                    } catch (UnsupportedFlavorException e) {
                    /*
                    try {
                        String processo = "";
                        String etiquetinha = "";
                        
                        clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                        flavor = DataFlavor.stringFlavor;
                        processo = clipboard.getData(flavor).toString();
                        //Necessário encontrar a metade da string e soma-la para andar adiante do vetor
                        processo = processo.toUpperCase();
                        processo = tratamento.removerAcento(processo);
                        System.out.println("Processo: " + processo);

                        //Recebe a etiqueta que foi identificada na triagem.
                        resultado = busca.find_JEF(processo, "DOC");
                        etiquetinha = resultado.getEtiqueta();
                        
                        System.out.println("================\n=============\n============");
                        System.out.println("resultado.getEtiqueta(): " + resultado.getEtiqueta());
                        System.out.println("etiquetinha: " + etiquetinha);
                                
                        if (etiquetinha.contains("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO")) {
                            for (int a = 0; a < 3; a++) {
                                WebElement Tela2 = driver.findElement(By.xpath("//*[@id=\"myiframe-body\"]"));
                                Tela2.click();
                                sobe(driver);
                                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0061')).perform();
                                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0063')).perform();
                                action.keyUp(Keys.CONTROL).build().perform();
                                Thread.sleep(2000);
                                /* Lib StringSelection pode estar causando erro no processo de triagem
                                stringSelection = new StringSelection("");
                                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
                                *//*
                                clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                                flavor = DataFlavor.stringFlavor;

                                processo = clipboard.getData(flavor).toString();
                                processo = processo.toUpperCase();
                                processo = tratamento.removerAcento(processo);

                                resultado = busca.find_JEF(processo, "DOC");
                                etiquetinha = resultado.getEtiqueta();
                                if (!etiquetinha.contains("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO")) {
                                    return resultado;
                                }
                            }
                        }

                    } catch (UnsupportedFlavorException e) {
                        */
                    } catch (IOException e) {
                    }
                } else {
                    resultado.setEtiqueta("ERRO EM TRIAGEM: PDF NÃO PESQUISÁVEL");
                    return resultado;
                }//Fim do ELSE de ERRO
            }//Fim do ELSE de triagem de DOCUMENTO
        }//Fim do ELSE IF de triagem de DOCUMENTO
        return resultado;
    }//Fecha o metodo copia_processo
/*
    public Resultado pericial(WebDriver driver) throws InterruptedException, SQLException, UnsupportedFlavorException, IOException {
        Resultado resultado = new Resultado();
        ClicaProcesso click = new ClicaProcesso();
        Tratamento tratamento = new Tratamento();
        Triagem busca = new Triagem();
        Actions action = new Actions(driver);

        //Verifica se a movimentação está pronta
        click.ClicaProcesso(driver);
        
        //Identifica as linhas da tabela de movimentação processual <rr> 
        WebElement TabelaTref = driver.findElement(By.id("treeview-1015-body"));
        List tarefasT = new ArrayList(TabelaTref.findElements(By.cssSelector("tr")));
        //FOR - Enquantou houve elementos na tabela, do último para o primeiro
        for (int i = tarefasT.size(); i >= 0; i--) {
            //IF - Busca pelas expressões descritas, dentro das <tr> da movimentação
            if (driver.findElement(By.xpath("//tr[" + i + "]/td[2]/div/span/span[1]")).getText().toUpperCase().contains("LAUDO PERICIAL")
                    || driver.findElement(By.xpath("//tr[" + i + "]/td[2]/div/span/span[1]")).getText().toUpperCase().contains("CERTIDÃO") ) {
                //Clica no <tr> identificado
                driver.findElement(By.xpath("//tr[" + i + "]/td/div")).click();
                //Armazena e clica no HTML onde é exibido o documento no Sapiens (TELA 2)
                driver.findElement(By.xpath("//*[@id=\"myiframe-body\"]")).click();
                //Action para dar Ctrl+a e Ctrl+c
                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0061')).perform();
                action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0063')).perform();
                //Var para armazenar o buffer dos elementos do documento identificado
                String BuscaPericial = "";
                //Armazena o buffer do teclado na variavel acima e busca o termo "PERICIAL"
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                DataFlavor flavor = DataFlavor.stringFlavor;
                BuscaPericial = clipboard.getData(flavor).toString();
                BuscaPericial = tratamento.removerAcento(BuscaPericial);
                
                //If - Verifica se existe o termo "Pericial" na var BuscaPericial para seguir a tragem especifica
                if (BuscaPericial.contains("PERICIAL")
                        || BuscaPericial.contains("PARECER")
                        || BuscaPericial.contains("SIMPLIFICADA")
                    ) { //CADASTRAR POSSIVEIS VERIFICAÇÕES
                    int LinhaAtual = Integer.parseInt(driver.findElement(By.xpath("//tr[" + i + "]/td/div")).getText()); //Armazena a linha do FRONT em que está a movimentação
                    int LinhaEsperada = LinhaAtual+1; //Armazena o valor que DEVERIA ser o seguite da linha no FRONT
                    int LinhaProxima = Integer.parseInt(driver.findElement(By.xpath("//tr[" + (i+1) + "]/td/div")).getText()); //Armazena o valor da PROXIMA linha do FRONT
                    if(LinhaEsperada!=LinhaProxima){
                        //Identifica e clica na SETA da movimentação para expandir os documentos
                        driver.findElement(By.xpath("//tr[" + i + "]/td[2]/div/img[1]")).click();
                        //Reconta a tabela de movimentação para considerar os arquivos expandidos na MESMA var que já estava sendo utilizada
                        tarefasT = new ArrayList(TabelaTref.findElements(By.cssSelector("tr")));
                        //FOR - Recontar a lista de movimentação até onde foi localizado a var i
                        for (int j = tarefasT.size(); j >= 0; j--) {

                            if (i==j){
                                //Var aqui para garantir o armazenamento da informação
                                int aqui = i+1;//Armazena a proxima LINHA a que deveria aparecer após a expansão da pasta
                                //Tempo de espera para garantir que a lista de movimentação será carregada
                                Thread.sleep(2000);
                                //Clica no elemento abaixo do identifiado no IF anterior
                                driver.findElement(By.xpath("//tr[" + aqui + "]/td/div")).click();
                                Thread.sleep(2000);
                                //Clica no HTML onde é exibido o documento no Sapiens (TELA 2)
                                driver.findElement(By.xpath("//*[@id=\"myiframe-body\"]")).click();
                                
                                //resultado = teste(driver);
                                
                                try {
                                    
                                    resultado.setLocal("LAUDO PERICIAL");
                                    resultado.setEtiqueta("NÃO FOI POSSÍVEL LOCALIZAR LAUDO PERICIAL");
                                    resultado.setRadical("");
                                    resultado.setComplemetno("");

                                    action = new Actions(driver);
                                    action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0061')).perform();
                                    action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0063')).perform();
                                    
                                    String etiquetinha = "";
                                    String processo = "";
                                    //Armazena o buffer do teclado na variavel acima e busca o termo "PERICIAL"
                                    Clipboard clipboard1 = Toolkit.getDefaultToolkit().getSystemClipboard();
                                    DataFlavor flavor1 = DataFlavor.stringFlavor;
                                    
                                    processo = clipboard1.getData(flavor1).toString();
                                    processo = tratamento.removerAcento(processo);

                                    //Recebe a etiqueta que foi identificada na triagem.
                                    resultado = busca.find_JEF(processo, "DOC");
                                    etiquetinha = resultado.getEtiqueta();
                                    
                                    if (etiquetinha.contains("NÃO FOI POSSÍVEL")) {
                                        for (int k = 0; k < 3; k++) {
                                            
                                            if (k==0){
                                                action.sendKeys(Keys.DOWN).build().perform();
                                            }
                                            
                                            //sobe(driver);
                                            Thread.sleep(2000);
                                            
                                            //Clica no HTML onde é exibido o documento no Sapiens
                                            driver.findElement(By.xpath("//*[@id=\"myiframe-body\"]")).click();

                                            action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0061')).perform();
                                            action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0063')).perform();
                                            Thread.sleep(2000);
  
                                            //Armazena o buffer do teclado na variavel acima e busca o termo "PERICIAL"
                                            clipboard1 = Toolkit.getDefaultToolkit().getSystemClipboard();
                                            flavor1 = DataFlavor.stringFlavor;

                                            processo = clipboard1.getData(flavor1).toString();
                                            processo = tratamento.removerAcento(processo);
                                            
                                            resultado = busca.find_JEF(processo, "DOC");
                                            etiquetinha = resultado.getEtiqueta();

                                            if (!etiquetinha.contains("NÃO FOI POSSÍVEL")) {
                                                return resultado;
                                            }
                                        }
                                        if (resultado.getEtiqueta().contains("NÃO FOI POSSÍVEL LOCALIZAR ASSUNTO OU COMPLEMENTO")) {
                                            resultado.setEtiqueta("NÃO FOI POSSÍVEL LOCALIZAR LAUDO PERICIAL");
                                            return resultado;
                                        }
                                        //return resultado; Acrescentado no return acima
                                    }
                                    return resultado;
                                } catch (UnsupportedFlavorException e) {
                                } catch (IOException e) {
                                }
                                
                                //} else {
                                    //resultado.setEtiqueta("ERRO EM TRIAGEM.");
                                //}//Fim do ELSE de ERRO
                                //return resultado;
                            } 

                        }//Fim do FOR do tamanho da lista de movimentação do Sapiens
                    }//Fim do IF de verificação de linha
                }//Fim do IF de verificação de busca para clicar na seta
            }//Fim do IF de identificação dos elementos setados para triagem especifica
        }//Fim do FOR do tamanho da lista de movimentação do Sapiens
        return resultado; //Retorna a etiqueta identificada
    } //Fecha o metodo pericial
*/
    
}//Fecha a classe

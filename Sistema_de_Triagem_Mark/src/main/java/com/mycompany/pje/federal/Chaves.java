/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import javax.swing.JOptionPane;

/**
 *
 * @author carlos.luan
 */
public class Chaves
{

    String radical = null;
    String complemento = null;
    String Etiqueta = null;
    String Tipo=null;
    private String Prioridade=null;

    public String getTipo()
    {
        return Tipo;
    }

    public void setTipo(String indice)
    {
        this.Tipo = indice;
    }

    public String getRadical()
    {
        return radical;
    }

    public void setRadical(String radical)
    {
        this.radical = radical;
    }

    public String getComplemento()
    {
        return complemento;
    }

    public void setComplemento(String complemento)
    {
        if (complemento != "")
        {
            this.complemento = complemento;
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Complemento não pode ser nulo!");
        }
    }

    public String getEtiqueta()
    {
        return Etiqueta;
    }

    public void setEtiqueta(String Etiqueta)
    {
        if (Etiqueta != "")
        {
            this.Etiqueta = Etiqueta;
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Etiqueta não pode ser nulo!");
        }
    }

    public String getPrioridade() {
        return Prioridade;
    }

    public void setPrioridade(String Prioridade) {
        if(Prioridade.equals("0")){
            Prioridade="1";
            this.Prioridade=Prioridade;
        }else{
        this.Prioridade=Prioridade;
        }
    }
    
}
